@php
    if(Auth::user()->avatar){
        $user_avatar = Voyager::image(Auth::user()->avatar);
    } else {
        $user_avatar = voyager_asset('icon/karo.svg');
    }

@endphp

<div class="sidebar">
    <div class="flex-box title-row">
{{--        <a  href="{{ route('voyager.profile') }}" class="logo flex-box">--}}
        <a  href="/kcp/users/{{ auth()->guard('web')->user()->id }}/edit" class="logo flex-box">
            <div class="logo-box flex-box">
                <img height="22px" src="{{ voyager_asset('icon/karo.svg') }}">
            </div>
            <div class="logo-text">
                <h5>
                    {{ __('voyager::generic.karo_panel') }}
                </h5>
                <p>
                    {{ __('voyager::generic.manage_account') }}
                </p>
            </div>
        </a>
        <a data-tooltip="خروح"  href="/kcp/logout" class="flex-box log-out">
            <img src="{{ voyager_asset('icon/logout.svg') }}">
        </a>
    </div>
    <nav class='animated sidebar-list'>

        <ul class="sidebar-list-item">

            {!! menu('admin', 'voyager::menu.sidebarmenu') !!}

        </ul>
    </nav>
</div>
