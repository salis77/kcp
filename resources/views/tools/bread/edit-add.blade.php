@extends('voyager::master')

@if (isset($dataType->id))
    @section('page_title', __('voyager::bread.edit_bread_for_table', ['table' => $dataType->name]))

@php
    $display_name = $dataType->display_name_singular;
    $display_name_plural = $dataType->display_name_plural;
@endphp

@else
    @section('page_title', __('voyager::bread.create_bread_for_table', ['table' => $table]))
@endif

@section('page_header')
    <div class="page-title">
        <i class="voyager-data"></i>
        @if (isset($dataType->id))
            {{ __('voyager::bread.edit_bread_for_table', ['table' => $dataType->name]) }}
        @else
            {{ __('voyager::bread.create_bread_for_table', ['table' => $table]) }}
        @endif
    </div>
    @php
        $isModelTranslatable = (!isset($isModelTranslatable) || !isset($dataType)) ? false : $isModelTranslatable;
        if (isset($dataType->name)) {
            $table = $dataType->name;
        }
    @endphp
    @include('voyager::multilingual.language-selector')
@stop

@section('content')

    <div class="row">
        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
            <nav class="navigation">
                <div class="right-top-nav flex-box">
                    <a class="flex-box back" href="{{ url()->previous() }}">
                        <img src="{{voyager_asset('icon/back.svg')}}">
                        {{ __('voyager::generic.return_to_list') }}

                    </a>
                    <button class="flex-box add-new" onclick="document.getElementById('contact_form').submit()">
                        <img src="{{voyager_asset('icon/change.svg')}}">
                        <p>
                            {{ __('voyager::generic.submit') }}

                        </p>

                    </button>
                </div>
            </nav>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="row">
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <h5 class="title">
                                {{ ucfirst($table) }}
                                {{ __('voyager::bread.bread_info') }}
                            </h5>
                        </div>

                        <form id="contact_form"
                              action="@if(isset($dataType->id)){{ route('voyager.bread.update', $dataType->id) }}@else{{ route('voyager.bread.store') }}@endif"
                              method="POST" role="form">
                            @if(isset($dataType->id))
                                <input type="hidden" value="{{ $dataType->id }}" name="id">
                                {{ method_field("PUT") }}
                            @endif
                            <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">

                                <div class="box">
                                    <div class="row">
                                        <div class="col-lg-8 col-md-12 col-sm-12">

                                            <!-- CSRF TOKEN -->
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
                                                    <div class="input-row">
                                                        <label>
                                                            {{ __('voyager::database.table_name') }}
                                                        </label>

                                                        <input type="text" readonly name="name"
                                                               placeholder="{{ __('generic_name') }}"
                                                               value="{{ $dataType->name ?? $table }}">
                                                    </div>
                                                </div>
                                                <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
                                                    <div class="input-row">
                                                        <label>
                                                            {{ __('voyager::bread.display_name_singular') }}
                                                        </label>
                                                        @if($isModelTranslatable)
                                                            @include('voyager::multilingual.input-hidden', [
                                                                'isModelTranslatable' => true,
                                                                '_field_name'         => 'display_name_singular',
                                                                '_field_trans' => get_field_translations($dataType, 'display_name_singular')
                                                            ])
                                                        @endif
                                                        <input type="text"
                                                               name="display_name_singular"
                                                               id="display_name_singular"
                                                               placeholder="{{ __('voyager::bread.display_name_singular') }}"
                                                               value="{{ $display_name }}">
                                                    </div>
                                                </div>
                                                <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
                                                    <div class="input-row">
                                                        <label>
                                                            {{ __('voyager::bread.display_name_plural') }}
                                                        </label>
                                                        @if($isModelTranslatable)
                                                            @include('voyager::multilingual.input-hidden', [
                                                                'isModelTranslatable' => true,
                                                                '_field_name'         => 'display_name_plural',
                                                                '_field_trans' => get_field_translations($dataType, 'display_name_plural')
                                                            ])
                                                        @endif
                                                        <input type="text"
                                                               name="display_name_plural"
                                                               id="display_name_plural"
                                                               placeholder="{{ __('voyager::bread.display_name_plural') }}"
                                                               value="{{ $display_name_plural }}">

                                                    </div>
                                                </div>
                                                <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
                                                    <div class="input-row">
                                                        <label>
                                                            {{ __('voyager::bread.url_slug') }}
                                                        </label>
                                                        <input type="text" name="slug"
                                                               placeholder="{{ __('voyager::bread.url_slug_ph') }}"
                                                               value="{{ $dataType->slug ?? $slug }}">

                                                    </div>
                                                </div>
                                                <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
                                                    <div class="input-row">
                                                        <label>{{ __('voyager::bread.icon_hint') }} <a
                                                                href="{{ route('voyager.compass.index', [], false) }}#fonts"
                                                                target="_blank">{{ __('voyager::bread.icon_hint2') }}
                                                            </a></label>
                                                        <input type="text" name="icon"
                                                               placeholder="{{ __('voyager::bread.icon_class') }}"
                                                               value="{{ $dataType->icon ?? '' }}">

                                                    </div>
                                                </div>
                                                <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
                                                    <div class="input-row">
                                                        <label>
                                                            {{ __('voyager::bread.model_name') }}
                                                            <span class="voyager-question"
                                                                  aria-hidden="true"
                                                                  data-toggle="tooltip"
                                                                  data-placement="right"
                                                                  title="{{ __('voyager::bread.model_name_ph') }}"></span>
                                                        </label>
                                                        <input type="text" name="model_name"
                                                               placeholder="{{ __('voyager::bread.model_class') }}"
                                                               value="{{ $dataType->model_name ?? $model_name }}">

                                                    </div>
                                                </div>
                                                <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
                                                    <div class="input-row">
                                                        <label>
                                                            {{ __('voyager::bread.controller_name') }}
                                                            <span class="voyager-question"
                                                                  aria-hidden="true"
                                                                  data-toggle="tooltip"
                                                                  data-placement="right"
                                                                  title="{{ __('voyager::bread.controller_name_hint') }}"></span>
                                                        </label>
                                                        <input type="text" name="controller"
                                                               placeholder="{{ __('voyager::bread.controller_name') }}"
                                                               value="{{ $dataType->controller ?? '' }}">

                                                    </div>
                                                </div>
                                                <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
                                                    <div class="input-row">
                                                        <label>
                                                            {{ __('voyager::bread.policy_name') }}

                                                            <span class="voyager-question"
                                                                  aria-hidden="true"
                                                                  data-toggle="tooltip"
                                                                  data-placement="right"
                                                                  title="{{ __('voyager::bread.policy_name_ph') }}"></span>
                                                        </label>
                                                        <input type="text" name="policy_name"
                                                               placeholder="{{ __('voyager::bread.policy_class') }}"
                                                               value="{{ $dataType->policy_name ?? '' }}">

                                                    </div>
                                                </div>

                                                <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
                                                    <div class="input-row">

                                                        <label>
                                                            {{ __('voyager::bread.order_column') }}
                                                            <span class="voyager-question"
                                                                  aria-hidden="true"
                                                                  data-toggle="tooltip"
                                                                  data-placement="right"
                                                                  title="{{ __('voyager::bread.order_column_ph') }}">

                                                            </span>
                                                        </label>
                                                        <div class="dropdown custom-selects">

                                                            <select>
                                                                <option value="">-- {{ __('voyager::generic.none') }}
                                                                    --
                                                                </option>

                                                                @foreach($fieldOptions as $tbl)
                                                                    <option value="{{ $tbl['field'] }}"
                                                                            @if(isset($dataType) && $dataType->order_column == $tbl['field']) selected @endif>
                                                                        {{ $tbl['field'] }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
                                                    <div class="input-row">
                                                        <label>
                                                            {{ __('voyager::bread.order_ident_column') }}
                                                            <span class="voyager-question"
                                                                  aria-hidden="true"
                                                                  data-toggle="tooltip"
                                                                  data-placement="right"
                                                                  title="{{ __('voyager::bread.order_ident_column_ph') }}">

                                                        </span>
                                                        </label>
                                                        <div class="dropdown custom-selects">
                                                            <select>


                                                                @foreach($fieldOptions as $tbl)
                                                                    <option value="{{ $tbl['field'] }}"
                                                                            @if(isset($dataType) && $dataType->order_display_column == $tbl['field']) selected @endif
                                                                    >{{ $tbl['field'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
                                                    <div class="input-row">
                                                        <label>
                                                            {{ __('voyager::bread.order_direction') }}

                                                        </label>
                                                        <div class="dropdown custom-selects">
                                                            <select>
                                                                <option value="asc"
                                                                        @if(isset($dataType) && $dataType->order_direction == 'asc') selected @endif>
                                                                    {{ __('voyager::generic.ascending') }}
                                                                </option>
                                                                <option value="desc"
                                                                        @if(isset($dataType) && $dataType->order_direction == 'desc') selected @endif>
                                                                    {{ __('voyager::generic.descending') }}
                                                                </option>
                                                            </select>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
                                                    <div class="input-row">
                                                        <label>
                                                            {{ __('voyager::bread.default_search_key') }}
                                                            <span class="voyager-question"
                                                                  aria-hidden="true"
                                                                  data-toggle="tooltip"
                                                                  data-placement="right"
                                                                  title="{{ __('voyager::bread.default_search_key_ph') }}"> </span>
                                                        </label>
                                                        <div class="dropdown custom-selects">
                                                            <select>
                                                                <option value="">-- {{ __('voyager::generic.none') }}
                                                                    --
                                                                </option>

                                                                @foreach($fieldOptions as $tbl)
                                                                    <option value="{{ $tbl['field'] }}"
                                                                            @if(isset($dataType) && $dataType->default_search_key == $tbl['field']) selected @endif
                                                                    >{{ $tbl['field'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                                                    <div class="flex-box check-box-row">

                                                        <label class="switch">
                                                            <h6>
                                                                {{ __('voyager::bread.generate_permissions') }}
                                                            </h6>
                                                            <?php $checked = (isset($dataType->generate_permissions) && $dataType->generate_permissions == 1) || (isset($generate_permissions) && $generate_permissions); ?>

                                                            <input type="checkbox"
                                                                   name="generate_permissions"
                                                                   data-on="{{ __('voyager::generic.yes') }}"
                                                                   data-off="{{ __('voyager::generic.no') }}"
                                                                   @if($checked) checked @endif hidden/>
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                                                    <div id="check-box-row" class="flex-box check-box-row">

                                                        <label class="switch">
                                                            <h6>
                                                                {{ __('voyager::bread.server_pagination') }}
                                                            </h6>
                                                            <?php $checked = (isset($dataType->server_side) && $dataType->server_side == 1) || (isset($server_side) && $server_side); ?>

                                                            <input hidden name="server_side"
                                                                   checked value="1">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                @if (isset($scopes) && isset($dataType))
                                                    <div
                                                        class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
                                                        <div class="input-row">
                                                            <label>
                                                                {{ __('voyager::bread.scope') }}

                                                            </label>
                                                            <div class="dropdown custom-selects">
                                                                <select>
                                                                    <option value="">
                                                                        -- {{ __('voyager::generic.none') }} --
                                                                    </option>
                                                                    @foreach($scopes as $scope)
                                                                        <option value="{{ $scope }}"
                                                                                @if($dataType->scope == $scope) selected @endif
                                                                        >{{ $scope }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class=" padding-item col-lg-12 col-md-12 col-sm-12">
                                <h5 class="title">
                                    {{ __('voyager::bread.edit_rows', ['table' => $table]) }}:
                                </h5>
                            </div>

                            <div class="padding-item col-lg-12 col-md-12 col-sm-12">

                                <div class="box">
                                    <div class="row">
                                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                                            <div class="headerOfTitleBread">
                                                <div class="col-md-3 col-xs-3 px-2 ">
                                                    {{ __('voyager::database.optional_details') }}
                                                </div>
                                                <div class="col-md-2 col-xs-2 px-2 ">
                                                    {{ __('voyager::bread.display_name') }}
                                                </div>
                                                <div class="col-md-2 col-xs-2 px-2">
                                                    {{ __('voyager::database.input_type') }}
                                                </div>
                                                <div class="col-md-2 col-xs-2 px-2">
                                                    {{ __('voyager::database.visibility') }}
                                                </div>
                                                <div class="col-md-2 col-xs-2 px-2">
                                                    {{ __('voyager::database.field') }}
                                                </div>

                                                <div class="col-md-1 col-xs-1 px-2">

                                                </div>
                                            </div>
                                            <table class="drag-table">
                                                <thead>
                                                <tr class="tr">
                                                    {{-- <th class="padding-item child2">
                                                         {{ __('voyager::database.field') }}
                                                     </th>
                                                     <th class="padding-item child2">
                                                         {{ __('voyager::database.visibility') }}
                                                     </th>
                                                     <th class="padding-item child2">
                                                         {{ __('voyager::database.input_type') }}
                                                     </th>
                                                     <th class="child1 padding-item text-center">
                                                         {{ __('voyager::bread.display_name') }}
                                                     </th>
                                                     <th class="child1 padding-item ">
                                                         {{ __('voyager::database.optional_details') }}
                                                     </th>
                                                     <th class="child1 padding-item">

                                                     </th>--}}
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <div class="drag-table" id="bread-items">

                                                    @php
                                                        $r_order = 0;
                                                    @endphp

                                                    @foreach($fieldOptions as $data)
                                                        @php
                                                            $r_order += 1;
                                                        @endphp

                                                        @if(isset($dataType->id))
                                                            <?php $dataRow = Voyager::model('DataRow')->where('data_type_id', '=', $dataType->id)->where('field', '=', $data['field'])->first(); ?>
                                                        @endif

                                                        <div class="tr row row-dd" style="z-index: {{ 100 - $loop->index }}; position: relative">


                                                            <div class="col-md-3 col-xs-3 px-2" style="margin-top: 12px">
                                                                <div class="alert alert-danger validation-error">
                                                                    {{ __('voyager::json.invalid') }}
                                                                </div>
                                                                <textarea
                                                                    id="json-input-{{ json_encode($data['field']) }}"
                                                                    class="resizable-editor"
                                                                    data-editor="json"
                                                                    name="field_details_{{ $data['field'] }}">
                                                                    {{ json_encode(isset($dataRow->details) ? $dataRow->details : new class{}) }}
                                                                </textarea>

                                                            </div>

                                                            <div class="col-md-2 col-xs-2 px-2">
                                                                <div class="input-row">
                                                                    @if($isModelTranslatable)
                                                                        @include('voyager::multilingual.input-hidden', [
                                                                            'isModelTranslatable' => true,
                                                                            '_field_name'         => 'field_display_name_' . $data['field'],
                                                                            '_field_trans' => $dataRow ? get_field_translations($dataRow, 'display_name') : $data['field'],
                                                                        ])
                                                                    @endif
                                                                    <input type="text" class="form-control"
                                                                           value="{{ $dataRow->display_name ?? ucwords(str_replace('_', ' ', $data['field'])) }}"
                                                                           name="field_display_name_{{ $data['field'] }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 col-xs-2 px-2">
                                                                <input type="hidden" name="field_{{ $data['field'] }}"
                                                                       value="{{ $data['field'] }}">
                                                                @if($data['type'] == 'timestamp')
                                                                    <p>{{ __('voyager::generic.timestamp') }}</p>
                                                                    <input type="hidden" value="timestamp"
                                                                           name="field_input_type_{{ $data['field'] }}">
                                                                @else
                                                                    <div class="dropdown custom-selects">
                                                                        <select
                                                                            name="field_input_type_{{ $data['field'] }}">
                                                                            <option value="">
                                                                                --
                                                                                {{ __('voyager::generic.none') }}
                                                                                --
                                                                            </option>
                                                                            @foreach (Voyager::formFields() as $formField)
                                                                                @php
                                                                                    $selected = (isset($dataRow->type) && $formField->getCodename() == $dataRow->type) || (!isset($dataRow->type) && $formField->getCodename() == 'text');
                                                                                @endphp
                                                                                <option
                                                                                    value="{{ $formField->getCodename() }}" {{ $selected ? 'selected' : '' }}>
                                                                                    {{ $formField->getName() }}
                                                                                </option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            <div class="col-md-2 col-xs-2 px-2">
                                                                <div class="flex-box">
                                                                    <div class="check-box edit-checkbox">
                                                                        <input class="my-checkbox" type="checkbox"
                                                                               id="field_browse_{{ $data['field'] }}"
                                                                               name="field_browse_{{ $data['field'] }}"
                                                                               @if(isset($dataRow->browse) && $dataRow->browse)
                                                                               checked="checked"
                                                                               @elseif($data['key'] == 'PRI')
                                                                               @elseif($data['type'] == 'timestamp' && $data['field'] == 'updated_at')
                                                                               @elseif(!isset($dataRow->browse))
                                                                               checked="checked"
                                                                            @endif />
                                                                        <label
                                                                            for="field_browse_{{ $data['field'] }}"><img
                                                                                src="{{voyager_asset('icon/white-checkbox.svg')}}"/></label>
                                                                    </div>
                                                                    <label
                                                                        for="field_browse_{{ $data['field'] }}">{{ __('voyager::generic.browse') }}</label>
                                                                </div>
                                                                {{--<input type="checkbox"
                                                                       id="field_browse_{{ $data['field'] }}"
                                                                       name="field_browse_{{ $data['field'] }}"
                                                                       @if(isset($dataRow->browse) && $dataRow->browse)
                                                                       checked="checked"
                                                                       @elseif($data['key'] == 'PRI')
                                                                       @elseif($data['type'] == 'timestamp' && $data['field'] == 'updated_at')
                                                                       @elseif(!isset($dataRow->browse))
                                                                       checked="checked"
                                                                    @endif />
                                                                <label
                                                                    for="field_browse_{{ $data['field'] }}">{{ __('voyager::generic.browse') }}</label>--}}

                                                                <div class="flex-box">
                                                                    <div class="check-box edit-checkbox">
                                                                        <input class="my-checkbox" type="checkbox"
                                                                               id="field_read_{{ $data['field'] }}"
                                                                               name="field_read_{{ $data['field'] }}"
                                                                               @if(isset($dataRow->read) && $dataRow->read) checked="checked"
                                                                               @elseif($data['key'] == 'PRI')@elseif($data['type'] == 'timestamp' && $data['field'] == 'updated_at')@elseif(!isset($dataRow->read)) checked="checked" @endif>

                                                                        <label
                                                                            for="field_read_{{ $data['field'] }}"><img
                                                                                src="{{voyager_asset('icon/white-checkbox.svg')}}"/></label>
                                                                    </div>
                                                                    <label
                                                                        for="field_read_{{ $data['field'] }}">{{ __('voyager::generic.read') }}</label>
                                                                </div>

                                                                {{--<input type="checkbox"
                                                                       id="field_read_{{ $data['field'] }}"
                                                                       name="field_read_{{ $data['field'] }}"
                                                                       @if(isset($dataRow->read) && $dataRow->read) checked="checked"
                                                                       @elseif($data['key'] == 'PRI')@elseif($data['type'] == 'timestamp' && $data['field'] == 'updated_at')@elseif(!isset($dataRow->read)) checked="checked" @endif>
                                                                <label
                                                                    for="field_read_{{ $data['field'] }}">{{ __('voyager::generic.read') }}</label>
                                                                <br/>--}}

                                                                <div class="flex-box">
                                                                    <div class="check-box edit-checkbox">
                                                                        <input class="my-checkbox" type="checkbox"
                                                                               id="field_edit_{{ $data['field'] }}"
                                                                               name="field_edit_{{ $data['field'] }}"
                                                                               @if(isset($dataRow->edit) && $dataRow->edit) checked="checked"
                                                                               @elseif($data['key'] == 'PRI')@elseif($data['type'] == 'timestamp' && $data['field'] == 'updated_at')@elseif(!isset($dataRow->edit)) checked="checked" @endif>

                                                                        <label
                                                                            for="field_edit_{{ $data['field'] }}"><img
                                                                                src="{{voyager_asset('icon/white-checkbox.svg')}}"/></label>
                                                                    </div>
                                                                    <label
                                                                        for="field_edit_{{ $data['field'] }}">{{ __('voyager::generic.edit') }}</label>
                                                                </div>

                                                                {{--<input type="checkbox"
                                                                       id="field_edit_{{ $data['field'] }}"
                                                                       name="field_edit_{{ $data['field'] }}"
                                                                       @if(isset($dataRow->edit) && $dataRow->edit) checked="checked"
                                                                       @elseif($data['key'] == 'PRI')@elseif($data['type'] == 'timestamp' && $data['field'] == 'updated_at')@elseif(!isset($dataRow->edit)) checked="checked" @endif>
                                                                <label
                                                                    for="field_edit_{{ $data['field'] }}">{{ __('voyager::generic.edit') }}</label>
                                                                <br/>--}}

                                                                <div class="flex-box">
                                                                    <div class="check-box edit-checkbox">
                                                                        <input class="my-checkbox" type="checkbox"
                                                                               id="field_add_{{ $data['field'] }}"
                                                                               name="field_add_{{ $data['field'] }}"
                                                                               @if(isset($dataRow->add) && $dataRow->add) checked="checked"
                                                                               @elseif($data['key'] == 'PRI')@elseif($data['type'] == 'timestamp' && $data['field'] == 'created_at')@elseif($data['type'] == 'timestamp' && $data['field'] == 'updated_at')@elseif(!isset($dataRow->add)) checked="checked" @endif>

                                                                        <label for="field_add_{{ $data['field'] }}"><img
                                                                                src="{{voyager_asset('icon/white-checkbox.svg')}}"/></label>
                                                                    </div>
                                                                    <label
                                                                        for="field_add_{{ $data['field'] }}">{{ __('voyager::generic.add') }}</label>
                                                                </div>
                                                                {{--<input type="checkbox"
                                                                       id="field_add_{{ $data['field'] }}"
                                                                       name="field_add_{{ $data['field'] }}"
                                                                       @if(isset($dataRow->add) && $dataRow->add) checked="checked"
                                                                       @elseif($data['key'] == 'PRI')@elseif($data['type'] == 'timestamp' && $data['field'] == 'created_at')@elseif($data['type'] == 'timestamp' && $data['field'] == 'updated_at')@elseif(!isset($dataRow->add)) checked="checked" @endif>
                                                                <label
                                                                    for="field_add_{{ $data['field'] }}">{{ __('voyager::generic.add') }}</label>
                                                                <br/>--}}


                                                                <div class="flex-box">
                                                                    <div class="check-box edit-checkbox">
                                                                        <input class="my-checkbox" type="checkbox"
                                                                               id="field_delete_{{ $data['field'] }}"
                                                                               name="field_delete_{{ $data['field'] }}"
                                                                               @if(isset($dataRow->delete) && $dataRow->delete) checked="checked"
                                                                               @elseif($data['key'] == 'PRI')@elseif($data['type'] == 'timestamp' && $data['field'] == 'updated_at')@elseif(!isset($dataRow->delete)) checked="checked" @endif>

                                                                        <label
                                                                            for="field_delete_{{ $data['field'] }}"><img
                                                                                src="{{voyager_asset('icon/white-checkbox.svg')}}"/></label>
                                                                    </div>
                                                                    <label
                                                                        for="field_delete_{{ $data['field'] }}">{{ __('voyager::generic.delete') }}</label>
                                                                </div>
                                                                {{--<input type="checkbox"
                                                                       id="field_delete_{{ $data['field'] }}"
                                                                       name="field_delete_{{ $data['field'] }}"
                                                                       @if(isset($dataRow->delete) && $dataRow->delete) checked="checked"
                                                                       @elseif($data['key'] == 'PRI')@elseif($data['type'] == 'timestamp' && $data['field'] == 'updated_at')@elseif(!isset($dataRow->delete)) checked="checked" @endif>
                                                                <label
                                                                    for="field_delete_{{ $data['field'] }}">{{ __('voyager::generic.delete') }}</label>
                                                                <br/>--}}
                                                            </div>

                                                            <div class="col-md-2 col-xs-2 px-2">
                                                                <p><strong>{{ $data['field'] }}</strong></p>
                                                                <p>

                                                                    <strong>{{ __('voyager::database.type') }}:</strong>
                                                                    <span>{{ $data['type'] }}</span>
                                                                </p>
                                                                <p>
                                                                    <strong>{{ __('voyager::database.key') }}:</strong>
                                                                    <span>{{ $data['key'] }}</span><br/>
                                                                </p>
                                                                <p>
                                                                    <strong>{{ __('voyager::generic.required') }}
                                                                        :</strong>
                                                                    @if($data['null'] == "NO")
                                                                        <span>{{ __('voyager::generic.yes') }}</span>
                                                                        <input type="hidden" value="1"
                                                                               name="field_required_{{ $data['field'] }}"
                                                                               checked="checked">
                                                                    @else
                                                                        <span>{{ __('voyager::generic.no') }}</span>
                                                                        <input type="hidden" value="0"
                                                                               name="field_required_{{ $data['field'] }}">
                                                                    @endif
                                                                </p>

                                                            </div>

                                                            <div class="col-md-1 col-xs-1 dragBreadMain">
                                                                <div class="handler voyager-handle drag-item">
                                                                    <img src="{{voyager_asset('icon/Hand,.svg')}}"/>
                                                                </div>
                                                                <input class="row_order" type="hidden"
                                                                       value="{{ $dataRow->order ?? $r_order }}"
                                                                       name="field_order_{{ $data['field'] }}">
                                                            </div>

                                                        </div>

                                                    @endforeach
                                                    @if(isset($dataTypeRelationships))
                                                        @foreach($dataTypeRelationships as $relationship)
                                                            @include('voyager::tools.bread.relationship-partial', $relationship)
                                                        @endforeach
                                                    @endif
                                                </div>


                                                </tbody>
                                            </table>

                                            <div class="padding-item margin-bottom col-lg-8 col-md-12 col-sm-12">
                                                <div class="add-row nav-button-row doc-button-row">

                                                    <a class="acsses add-item flex-box openModalNewRelation">
                                                        <img src="{{ voyager_asset('icon/Plus,%20Add.svg') }}">
                                                        {{ __('voyager::database.relationship.create') }}
                                                    </a>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('voyager::tools.bread.relationship-new-modal')

@stop

@section('javascript')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css">
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>

    <script>
        window.invalidEditors = [];
        var validationAlerts = $('.validation-error');
        validationAlerts.hide();
        $(function () {
            @if ($isModelTranslatable)
            /**
             * Multilingual setup
             */
            $('.layout .main .inside').multilingual({
                "form": 'form',
                "editing": true
            });
            @endif
            /**
             * Reorder items
             */
            reOrderItems();

            $('#bread-items').disableSelection();

            $('[data-toggle="tooltip"]').tooltip();

            $('.toggleswitch').bootstrapToggle();

            $('textarea[data-editor]').each(function () {
                var textarea = $(this),
                    mode = textarea.data('editor'),
                    editDiv = $('<div>').insertBefore(textarea),
                    editor = ace.edit(editDiv[0]),
                    _session = editor.getSession(),
                    valid = false;
                textarea.hide();

                // Validate JSON
                _session.on("changeAnnotation", function () {
                    valid = _session.getAnnotations().length ? false : true;
                    if (!valid) {
                        if (window.invalidEditors.indexOf(textarea.attr('id')) < 0) {
                            window.invalidEditors.push(textarea.attr('id'));
                        }
                    } else {
                        for (var i = window.invalidEditors.length - 1; i >= 0; i--) {
                            if (window.invalidEditors[i] == textarea.attr('id')) {
                                window.invalidEditors.splice(i, 1);
                            }
                        }
                    }
                });

                // Use workers only when needed
                editor.on('focus', function () {
                    _session.setUseWorker(true);
                });
                editor.on('blur', function () {
                    if (valid) {
                        textarea.siblings('.validation-error').hide();
                        _session.setUseWorker(false);
                        textarea.val(JSON.stringify(JSON.parse(_session.getValue())));
                    } else {
                        textarea.siblings('.validation-error').show();
                    }
                });

                _session.setUseWorker(false);

                editor.setAutoScrollEditorIntoView(true);
                editor.$blockScrolling = Infinity;
                editor.setOption("maxLines", 30);
                editor.setOption("minLines", 4);
                editor.setOption("showLineNumbers", false);
                editor.setTheme("ace/theme/github");
                _session.setMode("ace/mode/json");
                if (textarea.val()) {
                    _session.setValue(JSON.stringify(JSON.parse(textarea.val()), null, 4));
                }

                _session.setMode("ace/mode/" + mode);

                // copy back to textarea on form submit...
                textarea.closest('form').on('submit', function (ev) {
                    if (window.invalidEditors.length) {
                        ev.preventDefault();
                        ev.stopPropagation();
                        validationAlerts.hide();
                        for (var i = window.invalidEditors.length - 1; i >= 0; i--) {
                            $('#' + window.invalidEditors[i]).siblings('.validation-error').show();
                        }
                        toastr.error('{{ __('voyager::json.invalid_message') }}', '{{ __('voyager::json.validation_errors') }}', {
                            "preventDuplicates": true,
                            "preventOpenDuplicates": true
                        });
                    } else {
                        if (_session.getValue()) {
                            // uglify JSON object and update textarea for submit purposes
                            textarea.val(JSON.stringify(JSON.parse(_session.getValue())));
                        } else {
                            textarea.val('');
                        }
                    }
                });
            });

        });

        function reOrderItems() {
            $('#bread-items').sortable({
                delay: 0,
                scrollSpeed: 10,
                handle: '.handler',
                update: function (e, ui) {
                    var _rows = $('#bread-items').find('.row_order'),
                        _size = _rows.length;

                    for (var i = 0; i < _size; i++) {
                        $(_rows[i]).val(i + 1);
                    }
                },
                create: function (event, ui) {
                    sort();
                }
            });
        }

        function sort() {
            var sortableList = $('#bread-items');
            var listitems = $('div.row.row-dd', sortableList);

            listitems.sort(function (a, b) {
                return (parseInt($(a).find('.row_order').val()) > parseInt($(b).find('.row_order').val())) ? 1 : -1;
            });
            sortableList.append(listitems);

        }

        $('.openModalNewRelation').click(function () {

            $('#new_relationship_modal').modal('show');

        })

        /********** Relationship functionality **********/

        $(function () {
            $('.rowDrop').each(function () {
                populateRowsFromTable($(this));
            });

            $('.relationship_type').change(function () {
                if ($(this).val() == 'belongsTo') {
                    $(this).parents('.modal-body').find('.relationshipField').show();
                    $(this).parents('.modal-body').find('.relationshipPivot').hide();
                    $(this).parents('.modal-body').find('.relationship_key').show();
                    $(this).parents('.modal-body').find('.relationship_taggable').hide();
                    $(this).parents('.modal-body').find('.hasOneMany').removeClass('flexed');
                    $(this).parents('.modal-body').find('.belongsTo').addClass('flexed');
                } else if ($(this).val() == 'hasOne' || $(this).val() == 'hasMany') {
                    $(this).parents('.modal-body').find('.relationshipField').show();
                    $(this).parents('.modal-body').find('.relationshipPivot').hide();
                    $(this).parents('.modal-body').find('.relationship_key').hide();
                    $(this).parents('.modal-body').find('.relationship_taggable').hide();
                    $(this).parents('.modal-body').find('.hasOneMany').addClass('flexed');
                    $(this).parents('.modal-body').find('.belongsTo').removeClass('flexed');
                } else {
                    $(this).parents('.modal-body').find('.relationshipField').hide();
                    $(this).parents('.modal-body').find('.relationshipPivot').css('display', 'flex');
                    $(this).parents('.modal-body').find('.relationship_key').hide();
                    $(this).parents('.modal-body').find('.relationship_taggable').show();
                }
            });

            $('.btn-new-relationship').click(function () {
                $('#new_relationship_modal').modal('show');
            });

            relationshipTextDataBinding();

            $('.relationship_table').on('change', function () {
                var tbl_selected = $(this).val();
                var rowDropDowns = $(this).parents('.modal-body').find('.rowDrop');
                $(this).parents('.modal-body').find('.rowDrop').each(function () {
                    console.log('1');
                    $(this).data('table', tbl_selected);
                    populateRowsFromTable($(this));
                });
            });

            $('.voyager-relationship-details-btn').click(function () {
                $(this).toggleClass('open');
                if ($(this).hasClass('open')) {
                    $(this).parent().parent().find('.voyager-relationship-details').slideDown();
                } else {
                    $(this).parent().parent().find('.voyager-relationship-details').slideUp();
                }
            });

        });

        function populateRowsFromTable(dropdown) {
            var tbl = dropdown.data('table');
            var selected_value = dropdown.data('selected');
            if (tbl.length != 0) {
                $.get('{{ route('voyager.database.index') }}/' + tbl, function (data) {
                    $(dropdown).empty();
                    for (var option in data) {
                        $('<option/>', {
                            value: option,
                            html: option
                        }).appendTo($(dropdown));
                    }

                    if ($(dropdown).find('option[value="' + selected_value + '"]').length > 0) {
                        $(dropdown).val(selected_value);
                    }
                });
            }
        }

        function relationshipTextDataBinding() {
            $('.relationship_display_name').bind('input', function () {
                $(this).parents('.modal-body').find('.label_relationship p').text($(this).val());
            });
            $('.relationship_table').on('change', function () {
                var tbl_selected_text = $(this).find('option:selected').text();
                $(this).parents('.modal-body').find('.label_table_name').text(tbl_selected_text);
            });
            $('.relationship_table').each(function () {
                var tbl_selected_text = $(this).find('option:selected').text();
                $(this).parents('.modal-body').find('.label_table_name').text(tbl_selected_text);
            });
        }

        /********** End Relationship Functionality **********/
    </script>
@stop
