<textarea class="rte" name="{{ $row->field }}" id="richtext{{ $row->field }}" style="direction: ltr!important; height: 500px!important;">
    {{ old($row->field, $dataTypeContent->{$row->field} ?? '') }}
</textarea>


<script>
    $(document).ready(function (){

        var oldDataForEdit{{ $row->field }} = @json($dataTypeContent);
        var editor{{ $row->field }}Value = {};

        var currentLang = "en";
        var editor{{ $row->field }} = new RichTextEditor("#richtext{{ $row->field }}", {
            fontNameItems: 'Yekan Bakh',
            editorResizeMode: 'none',
            contentCssText: 'body{text-align: right; font-family: Yekan Bakh}'
        });
        let contentIn = $("#{{ $row->field }}_i18n");


        for (let i =0 ; i < $("input[name='i18n_selector']").length ; i++){
            const ele = $("input[name='i18n_selector']").eq(i);
            if(ele.attr("id") == 'en'){
                editor{{ $row->field }}Value["en"] = oldDataForEdit{{ $row->field }}["{{ $row->field }}"];
                editor{{ $row->field }}.setHTMLCode(oldDataForEdit{{ $row->field }}["{{ $row->field }}"])
            }else{
                editor{{ $row->field }}Value[ele.attr("id")] = "";
            }
        }


        for(let i= 0 ; i < oldDataForEdit{{ $row->field }}?.translations?.length ; i++){
            const ele = oldDataForEdit{{ $row->field }}.translations[i];
            if(ele.column_name == "{{ $row->field }}"){
                editor{{ $row->field }}Value[ele.locale] = ele.value;
            }
        }


        $( "form.form" ).submit(function( e ) {

            for (let i = 0 ; i < $("input[name='i18n_selector']").length +1 ; i++){
                if(i == $("input[name='i18n_selector']").length ){
                    $("input[name='i18n_selector']").eq(0).change();
                }else{
                    $("input[name='i18n_selector']").eq(i).change();
                }
            }

            contentIn.val(JSON.stringify(editor{{ $row->field }}Value));
        });

        $("input[name='i18n_selector']").change(function(){
            let selectedLang = $(this).attr("id");
            let contentVal = contentIn.val();

            contentVal = JSON.parse(contentVal);


            editor{{ $row->field }}Value[currentLang] = editor{{ $row->field }}.getHTMLCode();

            if(editor{{ $row->field }}Value[selectedLang]){
                editor{{ $row->field }}.setHTMLCode(editor{{ $row->field }}Value[selectedLang]);
            }else{
                editor{{ $row->field }}.setHTMLCode("");
            }


            currentLang = selectedLang;

            contentIn.val(JSON.stringify(editor{{ $row->field }}Value));
        })
    })


</script>
