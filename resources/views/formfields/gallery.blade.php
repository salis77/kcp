<div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
    <div class="input-row">
        <label>
            Upload Photo
        </label>
        <label id="fileimage" for="upload-imge" class="upload-file menu-items flex-box">
        <span class="flex-box">
            <img src="{{ voyager_asset('icon/upload-image.svg') }}">
            Upload Photo
        </span>
            <p>
                File Selection
            </p>

            <input @if($row->required == 1 && !isset($dataTypeContent->{$row->field})) required @endif type="file"
                   name="{{ $row->field }}" id="upload-imge" hidden accept="image/*">
            {{--        <input type="file" class="" multiple="" hidden="">--}}
        </label>
    </div>
    <div class="upload-image-row file-row margin-bottom">
    </div>
</div>

