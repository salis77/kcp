{{--<div class="check-box edit-checkbox">--}}

<?php $checked = false; ?>
@if(isset($dataTypeContent->{$row->field}) || old($row->field))
    <?php $checked = old($row->field, $dataTypeContent->{$row->field}); ?>
@else
    <?php $checked = isset($options->checked) &&
    filter_var($options->checked, FILTER_VALIDATE_BOOLEAN) ? true : false; ?>
@endif

<?php $class = $options->class ?? ""; ?>

@if(isset($options->on) && isset($options->off))
    {{--<input id="activityy1" type="checkbox" name="{{ $row->field }}" class="{{ $class }} my-checkbox"
           data-on="{{ $options->on }}" {!! $checked ? 'checked="checked"' : '' !!}
           data-off="{{ $options->off }}">
    <label for="activityy1">
        <img src="{{ voyager_asset('icon/white-checkbox.svg') }}">
    </label>--}}


    <div class="padding-item col-lg-12 col-md-12 col-sm-12" style="display: flex;width: unset;">
        <div class="flex-box check-box-row" style="float: left;margin: 0;padding: 0;border: none">

            <label class="switch" style="width: 100px;display: flex;justify-content: space-between;align-items: center">

                <input id="activityy1" type="checkbox" name="{{ $row->field }}" class="{{ $class }} my-checkbox"
                       data-on="{{ $options->on }}" {!! $checked ? 'checked="checked"' : '' !!}
                       data-off="{{ $options->off }}" hidden/>
                <span class="slider round" style="display: flex"></span>
            </label>
        </div>
    </div>



@else

    {{--        <input id="activityy" type="checkbox" name="{{ $row->field }}" class="{{ $class }}"--}}
    {{--               @if($checked) checked @endif>--}}
    {{--        <label for="activityy">--}}

    {{--            <img src="{{ voyager_asset('icon/white-checkbox.svg') }}">--}}
    {{--        </label>--}}

    <div class="padding-item col-lg-12 col-md-12 col-sm-12" style="display: flex;width: unset;">
        <div class="flex-box check-box-row" style="float: left;margin: 0;padding: 0;border: none">

            <label class="switch" style="width: 100px;display: flex;justify-content: space-between;align-items: center">

                <input id="activityy" type="checkbox" name="{{ $row->field }}" class="{{ $class }}"
                       @if($checked) checked @endif hidden/>
                <span class="slider round" style="display: flex"></span>
            </label>
        </div>
    </div>

@endif

{{--</div>--}}

