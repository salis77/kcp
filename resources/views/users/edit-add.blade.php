@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
@stop

@section('content')

    <div class="row">
        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
            <nav class="navigation">
                <div class="right-top-nav flex-box">
                    <a class="flex-box back" href="{{ url()->previous() }}">
                        <img src="{{voyager_asset('icon/back.svg')}}">
                        {{ __('voyager::generic.return_to_list') }}

                    </a>
                    <button class="flex-box add-new" onclick="document.getElementById('contact_form').submit()">
                        <img src="{{voyager_asset('icon/change.svg')}}">
                        <p>
                            {{__('voyager::hotdesk.user_update')}}
                        </p>
                    </button>
                </div>
            </nav>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12">
                    <div class="row">
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <h5 class="title">
                                {{__('voyager::hotdesk.user_edit')}}
                            </h5>
                        </div>
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert error">
                                    {{ $error }}
                                </div>
                            @endforeach
                        @endif
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="box">
                                <form id="contact_form" class="form edit-profile" role="form"
                                      action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                                      method="POST" enctype="multipart/form-data" autocomplete="off">
                                    <!-- PUT Method if we are editing -->
                                    @if(isset($dataTypeContent->id))
                                        {{ method_field("PUT") }}
                                    @endif
                                    {{ csrf_field() }}
                                    <div class="row">

                                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                                            <div class="edit-emage">
                                                <div class="imageWrapper">

                                                    @if(isset($dataTypeContent->avatar))
                                                        @if(filter_var($dataTypeContent->avatar, FILTER_VALIDATE_URL))
                                                            <img class="image" src="{{   $dataTypeContent->avatar }}"
                                                                 width="192px" height="192px"
                                                                 onerror="this.onerror=null;this.src='/assets/images/avatar_big.png';">
                                                        @else
                                                            <img class="image"
                                                                 src="{{ voyager_asset('icon/karo.svg') }}"
                                                                 style="background-color: var(--grey-bg); padding: 20px" width="192px"
                                                                 height="192px"
                                                                 onerror="this.onerror=null;this.src='/assets/images/avatar_big.png';">

                                                        @endif
                                                    @endif
                                                </div>
                                                <button type="button" class="file-upload flex-box">
                                                    <input type="file" class="file-input" data-name="avatar"
                                                           name="avatar" id="avatar_upload" accept="image/*">


                                                    <img src="{{voyager_asset('icon/camera.svg')}}">
                                                    {{ __('voyager::generic.upload_image') }}

                                                </button>
                                            </div>

                                        </div>

                                        <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                            <div class="input-row">
                                                <label>
                                                    {{ __('voyager::generic.name') }}
                                                </label>
                                                <input type="text" name="name"
                                                       value="{{ old('name', $dataTypeContent->name ?? '') }}"/>

                                            </div>

                                        </div>
                                        <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                            <div class="input-row email-input">
                                                <label>
                                                    {{ __('voyager::generic.email') }}
                                                </label>
                                                <input type="text" name="email"
                                                       value="{{ old('email', $dataTypeContent->email ?? '') }}"/>
                                                <img src="{{voyager_asset('icon/email.svg')}}">

                                            </div>

                                        </div>
                                        <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                            <div class="input-row">
                                                <label>
                                                    {{__('voyager::hotdesk.user_phone')}}
                                                </label>
                                                <input type="text" name="phone"
                                                       value="{{ old('phone', $dataTypeContent->phone ?? '') }}"
                                                       onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">

                                            </div>

                                        </div>
                                        @if((!is_null($dataTypeContent->getKey()) && Auth::user()->id != $dataTypeContent->id) || is_null($dataTypeContent->getKey()) || Auth::user()->role->type==0)

                                            <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                                <div class="input-row">
                                                    <label>
                                                        {{ __('voyager::profile.role_default') }}
                                                    </label>
                                                    @php
                                                        $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};

                                                        $row     = $dataTypeRows->where('field', 'user_belongsto_role_relationship')->first();
                                                        $options = $row->details;
                                                    @endphp
                                                    @php $relationshipField = $row->field; @endphp
                                                    <div class="dropdown custom-selects">
                                                        <select name="{{ $options->column }}">
                                                            @php
                                                                $model = app($options->model);
                                                                if(Auth::user()->role->type == 1)
                                                                  $query = $model::whereIn('type',[1,2])->get();
                                                                else
                                                                  $query = $model::get();
                                                            @endphp
                                                            @if(!$row->required)
                                                                <option
                                                                    value="">{{__('voyager::generic.none')}}</option>
                                                            @endif

                                                            @foreach($query as $relationshipData)
                                                                <option value="{{ $relationshipData->{$options->key} }}"
                                                                        @if($dataTypeContent->{$options->column} == $relationshipData->{$options->key}) selected="selected" @endif>{{ $relationshipData->{$options->label} }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        @php
                                            if (isset($dataTypeContent->locale)) {
                                                $selected_locale = $dataTypeContent->locale;
                                            } else {
                                                $selected_locale = config('app.locale', 'en');
                                            }

                                        @endphp
                                        <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                            <div class="input-row">
                                                <label>
                                                    {{ __('voyager::generic.locale') }}
                                                </label>
                                                <div class="dropdown custom-selects">
                                                    <select id="locale" name="locale">
                                                        @foreach (Voyager::getLocales() as $locale)
                                                            <option value="{{ $locale }}"
                                                                {{ ($locale == $selected_locale ? 'selected' : '') }}>{{ $locale }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                            <div class="input-row email-input">
                                                <label>
                                                    {{ __('voyager::generic.password') }}
                                                </label>
                                                <input type="password" id="user_password" name="password" value=""
                                                       autocomplete="new-password"/>

                                                <img src="{{voyager_asset('icon/pass.svg')}}">
                                                <div class="toggler show-pass flex-box">
                                                    <img src="{{voyager_asset('icon/show-pass.svg')}}">
                                                    <img class="hide" src="{{voyager_asset('icon/hide-pass.svg')}}">
                                                </div>
                                                <div class="flex-box pass-strenght">
                                                    <div class="strnght-line">
                                                        <div id="strength">
                                                            <div class="strenge-box box1"></div>
                                                            <div class="strenge-box box2"></div>
                                                            <div class="strenge-box box3"></div>
                                                            <div class="strenge-box box4"></div>
                                                            <div class="result-massage">
                                                                <p class="mainresult result"></p>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                        <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                            <div class="input-row email-input">
                                                <label>
                                                    {{__('voyager::hotdesk.user_repassword')}}
                                                </label>
                                                <input id="verify-user_password" type="password" data-re="password"
                                                       name="repassword"/>

                                                <img src="{{voyager_asset('icon/pass.svg')}}">
                                                <div class="verify-toggler show-pass flex-box">
                                                    <img src="{{voyager_asset('icon/show-pass.svg')}}">
                                                    <img class="hide" src="{{voyager_asset('icon/hide-pass.svg')}}">
                                                </div>
                                                <div class="flex-box pass-strenght">
                                                    <div class="strnght-line">
                                                        <div id="verify-strength">
                                                            <div class="strenge-box box1"></div>
                                                            <div class="strenge-box box2"></div>
                                                            <div class="strenge-box box3"></div>
                                                            <div class="strenge-box box4"></div>
                                                            <div class="result-massage">
                                                                <p class="second-result result"></p>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="row">
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <h5 class="title">
                                {{ __('voyager::generic.guide') }}
                            </h5>
                        </div>
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <p class="Description">
                                {{ __('voyager::generic.lorem_ipsum') }}

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('javascript')
    {{--    <script>--}}
    {{--        $('document').ready(function () {--}}
    {{--            $('.toggleswitch').bootstrapToggle();--}}
    {{--        });--}}
    {{--    </script>--}}

    <script>
        $('.file-input').change(function () {
            var curElement = $('.image');
            console.log(curElement);
            var reader = new FileReader();

            reader.onload = function (e) {
                // get loaded data and render thumbnail.
                curElement.attr('src', e.target.result);
            };

            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        });
    </script>
    <script>
        // Dynamic Password Strength Validation
        var result = $("#strength");
        $('#user_password').keyup(function () {
            $(".mainresult").html(maincheckStrength($('#user_password').val()))
        })

        function maincheckStrength(password) {

            //initial strength
            var strength = 0

            if (password.length == 0) {
                result.removeClass()
                $('#user_password').removeClass("weak")
                return ''
            }
            //if the password length is less than 6, return message.
            if (password.length < 6) {
                result.removeClass()
                $('#user_password').addClass("weak")
                result.addClass('short')
                return '{{ __('voyager::generic.low_password') }}'
            }

            //length is ok, lets continue.

            //if length is 8 characters or more, increase strength value
            if (password.length > 7) strength += 1

            //if password contains both lower and uppercase characters, increase strength value
            if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1

            //if it has numbers and characters, increase strength value
            if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1

            //if it has one special character, increase strength value
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1

            //if it has two special characters, increase strength value
            if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,",%,&,@,#,$,^,*,?,_,~])/)) strength += 1

            //now we have calculated strength value, we can return messages

            //if value is less than 2
            if (strength < 2) {
                result.removeClass()
                result.addClass('weak')
                $('#user_password').removeClass("weak")
                return '{{ __('voyager::generic.weak_password') }}'
            } else if (strength == 2) {
                result.removeClass()
                result.addClass('good')

                return '{{ __('voyager::generic.good_password') }}'
            } else {
                result.removeClass()
                result.addClass('strong')
                return '{{ __('voyager::generic.strong_password') }}'
            }
        }
    </script>
    <script>
        // Dynamic Password Strength Validation
        var secondresult = $("#verify-strength");
        $('#verify-user_password').keyup(function () {
            $(".second-result").html(checkStrength($('#verify-user_password').val()))
        })

        function checkStrength(password) {

            //initial strength
            var strength2 = 0

            if (password.length == 0) {
                secondresult.removeClass()
                $('#verify-user_password').removeClass("weak")
                return ''
            }
            //if the password length is less than 6, return message.
            if (password.length < 8) {
                secondresult.removeClass()
                $('#verify-user_password').addClass("weak")
                secondresult.addClass('short')
                return '{{ __('voyager::generic.low_password') }}'
            }

            //length is ok, lets continue.

            //if length is 8 characters or more, increase strength value
            if (password.length > 7) strength2 += 1

            //if password contains both lower and uppercase characters, increase strength value
            if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength2 += 1

            //if it has numbers and characters, increase strength value
            if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength2 += 1

            //if it has one special character, increase strength value
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength2 += 1

            //if it has two special characters, increase strength value
            if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,",%,&,@,#,$,^,*,?,_,~])/)) strength2 += 1

            //now we have calculated strength value, we can return messages

            //if value is less than 2
            if (strength2 < 2) {
                secondresult.removeClass()
                secondresult.addClass('weak')
                $('#verify-user_password').removeClass("weak")
                return '{{ __('voyager::generic.weak_password') }}'
            } else if (strength2 == 2) {
                secondresult.removeClass()
                secondresult.addClass('good')

                return '{{ __('voyager::generic.good_password') }}'
            } else {
                secondresult.removeClass()
                secondresult.addClass('strong')
                return '{{ __('voyager::generic.strong_password') }}'
            }
        }
    </script>
    <script>

        $('.toggler').click(function () {
            if ('password' == $('#user_password').attr('type')) {
                $('#user_password').prop('type', 'text');
                $(this).addClass('active');
            } else {
                $('#user_password').prop('type', 'password');
                $(this).removeClass('active');
            }
        });
        $('.verify-toggler').click(function () {
            if ('password' == $('#verify-user_password').attr('type')) {
                $('#verify-user_password').prop('type', 'text');
                $(this).addClass('active');
            } else {
                $('#verify-user_password').prop('type', 'password');
                $(this).removeClass('active');
            }
        });
    </script>
    <script>

        $(document).ready(function () {
            $('#verify-user_password').on('keyup', function () {
                var fst = $("#user_password").val();
                var sec = $("#verify-user_password").val();
                if (fst == sec) {
                    return true;
                } else {
                    $('#verify-user_password').addClass("weak")
                }
            });
        });

    </script>
@stop
