
<div style="margin-bottom: 10px" class="flex-box justify-content-between padding-item col-lg-12 col-md-12 col-sm-12">
    <h5 class="title">
        {{ __('voyager::compass.logs.title') }}

    </h5>
</div>

<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="row">
        <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
            <div class="box ">
                <table class="padding-item">
                    <thead>
                    <tr class="tr">
                        <th class="child2">
                            {{ __('voyager::compass.logs.level') }}

                        </th>
                        <th class="child2 text-center">
                            {{ __('voyager::compass.logs.date') }}

                        </th>
                        <th class="child4 text-center">
                            {{ __('voyager::compass.logs.content') }}

                        </th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($logs as $key => $log)

                        <tr class="tr">
                        <td class="child2" data-column="نوع خطا">
                            <div class="error-type red-text flex-box justify-content-start">
                                <img src="{{voyager_asset('icon/Warning.svg')}}">
                                {{$log['level']}}
                            </div>
                        </td>
                        <td class="child2 text-center" data-column="تاریخ ">
                            <div class="acsses">
                                {{{$log['date']}}}
                            </div>
                        </td>

                        <td class="child4 text-center" data-column="محتوا" style="width: 540px;display: block;line-break: anywhere; padding-right: 30px">

                                @if ($log['stack']) <a class="pull-right expand btn btn-default btn-xs"
                                                       data-display="stack{{{$key}}}"><span
                                        class="glyphicon glyphicon-search"></span></a>@endif
                                {{{$log['text']}}}
                                @if (isset($log['in_file'])) <br/>{{{$log['in_file']}}}@endif
                                @if ($log['stack'])
                                    <div class="stack" id="stack{{{$key}}}"
                                         style="display: none; white-space: pre-wrap;">{{{ trim($log['stack']) }}}
                                    </div>@endif

                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                </table>
{{--
                <div class="padding-item panigation">
                    <div class=" refrence">
                        ۸ از ۲۱۰۳ نتیجه
                    </div>
                    <a class="shrink Ellipse active">
                        ۱
                    </a>
                    <a class="shrink Ellipse">
                        ۲
                    </a>
                    <a class="shrink Ellipse ">
                        ۳
                    </a>
                    <a class="shrink Ellipse">
                        ...
                    </a>
                    <div class="Ellipse custom-selects" >
                        <select>
                            <option value="0">۸</option>
                            <option value="1">۱</option>
                            <option value="2">۲</option>
                            <option value="3">۳</option>
                            <option value="4">۴</option>
                        </select>
                    </div>

                </div>
--}}
            </div>
        </div>

    </div>
</div>
