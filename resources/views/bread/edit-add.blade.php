@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{voyager_asset('style/rte_theme_default.css')}}"/>
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    @include('voyager::multilingual.language-selector')
@endsection

@section('content')

    <div class="row layout">
        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
            <nav class="navigation">
                <div class="right-top-nav flex-box">
                    <a class="flex-box back" href="{{ url()->previous() }}">
                        <img src="{{voyager_asset('icon/back.svg')}}">
                        {{ __('voyager::generic.return_to_list') }}

                    </a>

                </div>
            </nav>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 main">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 inside">
                    <div class="row">
                        @include('voyager::multilingual.language-selector')

                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="box">
                                <!-- form start -->
                                <form role="form"
                                      class="form"
                                      action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                                      method="POST" enctype="multipart/form-data">
                                    <!-- PUT Method if we are editing -->
                                    <div class="row">

                                    @if($edit)
                                        {{ method_field("PUT") }}
                                    @endif

                                    <!-- CSRF TOKEN -->
                                        {{ csrf_field() }}
                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif

                                    <!-- Adding / Editing -->
                                        @php
                                            $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                                        @endphp

                                        @foreach($dataTypeRows as $row)
                                        <!-- GET THE DISPLAY OPTIONS -->
                                            @php
                                                $display_options = $row->details->display ?? NULL;
                                                if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                                    $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                                }
                                            @endphp
                                            @if (isset($row->details->legend) && isset($row->details->legend->text))
                                                <legend class="text-{{ $row->details->legend->align ?? 'center' }}"
                                                        style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                            @endif
                                            <div class="col-md-{{ $display_options->width ?? 12 }} col-12 col-xs-12"
                                                 @if($row->type == "checkbox")  style="border-radius: 8px;border: 1px solid var(--input-border);margin-top: 12px;margin-bottom: 12px;padding-top: 6px;padding-bottom: 6px;" @endif>


                                                <div
                                                    style="background-color: unset; @if($row->type != 'image')padding: 0 10px; @endif
                                                    @if($row->type == "checkbox") display: flex;justify-content: space-between;flex-direction: row-reverse; @endif"
                                                    class=" input-row form-group gray @if($row->type == 'select_dropdown' || $row->type == 'select_multiple' || $row->type == 'relationship') select @endif @if($row->type == 'hidden') hidden @endif {{ $errors->has($row->field) ? 'invalid' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                                    {{ $row->slugify }}
                                                    <label @if(Auth::user()->Locale =='fa') style="text-align: right;" @endif class="control-label" for="name">
                                                        {{ $row->getTranslatedAttribute('display_name') }}
                                                    </label>
                                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                                    @if (isset($row->details->view))
                                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options' => $row->details])
                                                    @elseif ($row->type == 'relationship')
                                                        @include('voyager::formfields.relationship', ['options' => $row->details])
                                                    @else

                                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                                    @endif

                                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                                    @endforeach
                                                    @if ($errors->has($row->field))
                                                        @foreach ($errors->get($row->field) as $error)
                                                            <div class="invalid-feedback">{{ $error }}</div>
                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                            @if($display_options && isset($display_options->clear) && $display_options->clear == true)
                                                <div class="clear"></div>
                                            @endif
                                        @endforeach
                                        <div class="clear"></div>
                                        <div class="button-group right">
                                            @section('submit-buttons')
                                                <button type="submit" class="flex-box add-new" style="margin-top: 10px">
                                                    <img src="{{voyager_asset('icon/change.svg')}}">
                                                    <p>

                                                        {{ __('voyager::generic.save') }}
                                                    </p>

                                                </button>
                                            @stop
                                            @yield('submit-buttons')
                                        </div>
                                    </div>
                                </form>

                                <iframe id="form_target" name="form_target" style="display:none"></iframe>
                                <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target"
                                      method="post"
                                      enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                                    <input name="image" id="upload_file" type="file"
                                           onchange="$('#my_form').submit();this.value='';">
                                    <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                                    {{ csrf_field() }}
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                {{--
                                <div class="col-lg-4 col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                                            <h5 class="title">
                                                {{ __('voyager::generic.guide') }}
                                            </h5>
                                        </div>
                                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                                            <p class="Description">
                                                {{ __('voyager::generic.lorem_ipsum') }}

                                            </p>
                                        </div>
                                    </div>
                                </div>
                --}}
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title">
                        {{ __('voyager::generic.are_you_sure') }}
                    </h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'
                    </h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger"
                            id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript" src="{{voyager_asset('js/rte.js')}}"></script>
    <script type="text/javascript" src="{{voyager_asset('js/all_plugins.js')}}"></script>


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/themes/nano.min.css"/>
    <link href="{{ voyager_asset('css/jalalidatepicker.min.css') }}" rel="stylesheet">


    <script src="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/pickr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/pickr.es5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="{{ voyager_asset('js/jalalidatepicker.min.js') }}"></script>


    <script>

        let need_resize = 0;

        $('body').on('DOMNodeInserted', function (e) {


            $(".select2-dropdown--below").ready(function () {


                const drop_down = $(".select2-dropdown");

                console.log(drop_down)

                if (drop_down) {

                    need_resize++;
                    if (need_resize === 1) {

                        if ($(".select2-selection--single").width()) {
                            // drop_down.width($(".select2-selection--single").width()).css('margin-top', "20px")
                            drop_down.css({'width':$(".select2-selection--single").width(),'margin-top':"20px"})
                        } else {
                            console.log(drop_down)

                            drop_down.css({'width':$(".select2-selection--multiple").width()+40,'margin-top':"20px"})
                        }
                    } else if (need_resize === 2)
                        need_resize = 0;
                } else
                    need_resize = 0;
            });


        });

        $(window).scroll(function (event) {
            $(".select2-dropdown--below").ready(function () {

                const drop_down = $(".select2-dropdown");

                if (drop_down) {
                    need_resize++;
                    if (need_resize === 1) {
                        if ($(".select2-selection--single")) {
                            drop_down.width($(".select2-selection--single").width()).css('margin-top', "20px")
                            drop_down.css({'width':$(".select2-selection--multiple").width()+40,'margin-top':"20px"})
                        } else {
                            drop_down.width($(".select2-selection--multiple").width()).css('margin-top', "20px")
                        }
                    } else if (need_resize === 2)
                        need_resize = 0;
                } else
                    need_resize = 0;
            });


        })

        $(".mySelectionItem").click(function () {


        })


        $('#upload-file-btn').on('change', function (e) {
            var fileName = e.target.files[0].name;

            $(".upload-file-row2").append("<div class=\"acsses  files\">" +
                "<div class=\"flex-box\">" +
                "<img class='close-tag' src=\"{{ voyager_asset('icon/clos.svg') }}\">"
                + fileName +
                "</div>" +
                "<div class=\"flex-box upload-prosses\">" +
                // "<p>Uploading ..</p>" +
                // "<p class=\"blue-text\">10 ٪</p>" +
                "</div>" +
                "</div>");
            $('.close-tag').click(function () {
                $(this).parent().parent().fadeOut();
            });

        });
        $('.delet-file').click(function () {
            $('.upload-file-row2 .files').fadeOut();
        });


        $('#upload-file').on('change', function () {
            var filename = $(this).val().replace(/.*(\/|\\)/, '');
            $(".upload-file-row").append("<div class=\"acsses  files\">" +
                "<div class=\"flex-box\">" +
                "<img class='close-tag' src=\"{{ voyager_asset('icon/clos.svg') }}\">"
                + filename +
                "</div>" +
                "<div class=\"flex-box upload-prosses\">" +
                /* "<p>Uploading ..</p>" +
                 "<p class=\"blue-text\">10 ٪</p>" +*/
                "</div>" +
                "</div>");
            $('.close-tag').click(function () {
                $(this).parent().parent().fadeOut();
            });
        });


        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();


                reader.onload = function (e) {
                    var $googleURL = e.target.result;
                    var previewClassNameBox = $(input).data("preview")
                    $("."+previewClassNameBox).html("<div class=\"acsses image-file files\">" +
                        "<div class=\"flex-box\">" +
                        "<img class='close-tag' src=\"{{ voyager_asset('icon/clos.svg') }}\">" +
                        "<div class='image-box'>" +
                        "<img id='upload-image-item' src='" + $googleURL + "'>" +
                        "</div>" +
                        "</div>" +
                        "<div class=\"flex-box upload-prosses\">" +
                        /* "<p>Uploading ..</p>" +
                         "<p class=\"blue-text\">10 ٪</p>" +*/
                        "</div>" +
                        "</div>");
                    $('.close-tag').click(function () {
                        $(this).parent().parent().fadeOut();
                    });
                }

                reader.readAsDataURL(input.files[0]);
            }

        }

        $(".uploadImageInput").change(function () {

            readURL(this);
        })


    </script>

    <script>
        const inputElement = document.querySelector('.color-picker-wrapper');

        const pickr = new Pickr({
            el: '#color-picker1',
            theme: 'nano',
            default: '#000',
            swatches: [
                'rgba(244, 67, 54, 1)',
                'rgba(233, 30, 99, 1)',
                'rgba(156, 39, 176,1)',
                'rgba(103, 58, 183, 1)',
                'rgba(63, 81, 181, 1)',
                'rgba(33, 150, 243,1)',
                'rgba(3, 169, 244,1)',
                'rgba(0, 188, 212, 1)',
                'rgba(0, 150, 136,1)',
                'rgba(76, 175, 80, 1)',
                'rgba(139, 195, 74, 1)',
                'rgba(205, 220, 57, 1)',
                'rgba(255, 235, 59,1)',
                'rgba(255, 193, 7, 1)'
            ],
            i18n: {

                'btn:save': 'ثبت',
                'btn:cancel': 'کنسل',
                'btn:clear': 'کنسل',
                'aria:btn:save': 'ثبت',
                'aria:btn:cancel': 'کنسل',
                'aria:btn:clear': 'کنسل',

            },

            components: {

                // Main components
                preview: true,
                opacity: true,
                hue: true,

                // Input / output Options
                interaction: {
                    hex: false,
                    rgba: false,
                    hsla: false,
                    hsva: false,
                    cmyk: false,
                    input: true,
                    clear: true,
                    save: true
                },

            },

        }).on('init', pickr => {
            inputElement.value = pickr.getSelectedColor().toRGBA().toString(0);
        }).on('save', color => {
            inputElement.value = color.toRGBA().toString(0);
            pickr.hide();
            var colorcode = $('.pcr-result').val();
            $('.colorcode').html(colorcode);
            $(".hiddenColor").val(colorcode);

        })

    </script>

    <script>
        jalaliDatepicker.startWatch({
            minDate: "attr",
            maxDate: "attr"
        });
        setTimeout(function () {
            var elm = document.getElementById("data-picker")[0];
            elm.focus();
            jalaliDatepicker.hide();
            jalaliDatepicker.show(elm);
        }, 1000);

        jalaliDatepicker.startWatch({
            minDate: "attr",
            maxDate: "attr"
        });

        function changeDatePicker() {
            console.log($("#data-picker2"))

        }

        setTimeout(function () {
            var elm = document.getElementById("data-picker2")[0];
            elm.focus();
            jalaliDatepicker.hide();
            jalaliDatepicker.show(elm);

            // $().change(function(){
            // })
        }, 1000);
    </script>

    <script>
        // new RichTextEditor(".rte", {
        //     fontNameItems: 'Yekan Bakh',
        //     editorResizeMode: 'none',
        //     contentCssText: 'body{text-align: right; font-family: Yekan Bakh}'
        // });


        window.onload = function () {
            let ifram = document.getElementsByTagName('iframe')[0];
            let minHeight = parseFloat(ifram.style.minHeight.replaceAll('px', ''));
            ifram.style.minHeight = (minHeight + 100) + 'px';
        };


    </script>
    <script>
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
            return function () {
                $file = $(this).siblings(tag);

                params = {
                    slug: '{{ $dataType->slug }}',
                    filename: $file.data('file-name'),
                    id: $file.data('id'),
                    field: $file.parent().data('field-name'),
                    multi: isMulti,
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text(params.filename);
                $('#confirm_delete_modal').modal('show');
            };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: ['YYYY-MM-DD']
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
            $('.layout .main .inside').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function (i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function () {
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if (response
                        && response.data
                        && response.data.status
                        && response.data.status == 200) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function () {
                            $(this).remove();
                        })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });

        const dateInput = document.getElementsByClassName("datePickerInput");
        // console.log(dateInput);

        dateInput.forEach(function (item , index){
            item.addEventListener("click", function () {
                console.log(item)
                try {
                    item.showPicker();
                    // A date picker is shown.
                } catch (error) {
                    // Use external library when this fails.
                }
            });
        })
    </script>
@endsection
