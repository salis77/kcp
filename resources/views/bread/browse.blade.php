@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

{{--
@section('page_header')
    <div class="action">
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-blue add">
                {{ __('voyager::generic.add_new') }}
            </a>
        @endcan
        @can('delete', app($dataType->model_name))
            @include('voyager::partials.bulk-delete')
        @endcan
        @can('edit', app($dataType->model_name))
            @if(isset($dataType->order_column) && isset($dataType->order_display_column))
                <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-blue">
                    <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
                </a>
            @endif
        @endcan
        @can('delete', app($dataType->model_name))
            @if($usesSoftDeletes)
                <input type="checkbox" @if ($showSoftDeleted) checked @endif id="show_soft_deletes" data-toggle="toggle"
                       data-on="{{ __('voyager::bread.soft_deletes_off') }}"
                       data-off="{{ __('voyager::bread.soft_deletes_on') }}">
            @endif
        @endcan
        @foreach($actions as $action)
            @if (method_exists($action, 'massAction'))
                @include('voyager::bread.partials.actions', ['action' => $action, 'data' => null])
            @endif
        @endforeach
        @include('voyager::multilingual.language-selector')
    </div>
@stop
--}}

@section('content')

    <div class="row">
        @if($dataTypeContent->count() > 0)

            <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                <nav class="navigation">
                    <div class="right-top-nav flex-box">

                        @if ($isServerSide)
                            <form class="web-item" method="get">
                                <img class="arrow" src="{{voyager_asset('icon/arrow.svg')}}">
                                <img class="search" src="{{voyager_asset('icon/search.svg')}}">
                                <div class="search-box">
                                    <input class="search-input" type="text"
                                           placeholder="{{ __('voyager::generic.search') }}" name="s"
                                           value="{{ $search->value }}">
                                    <div class="search-details">
                                        <div class="row">
                                            <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                                <div class=" input-row ">
                                                    <div class="dropdown custom-selects">

                                                        <select class="select-items " id="search_key" name="key"
                                                                data-placeholder="{{__('voyager::hotdesk.content_search_key')}}">
                                                            <option>
                                                                {{ __('voyager::generic.none') }}
                                                            </option>
                                                            @foreach($searchNames as $key => $name)
                                                                <option value="{{ $key }}"
                                                                        @if($search->key == $key || (empty($search->key) && $key == $defaultSearchKey)) selected @endif>{{ $name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                                <div class=" input-row ">
                                                    <div class="dropdown custom-selects">
                                                        <select class="select-items " id="filter" name="filter"
                                                                data-placeholder="{{__('voyager::hotdesk.content_search_filter')}}"
                                                                data-minimum-results-for-search="Infinity">
                                                            <option>
                                                                {{ __('voyager::generic.none') }}
                                                            </option>
                                                            <option value="contains"
                                                                    @if($search->filter == "contains") selected @endif>
                                                                contains
                                                            </option>
                                                            <option value="equals"
                                                                    @if($search->filter == "equals") selected @endif>=
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                    </div>
                                    <div class="search-details-bottom-item">
                                        <button class="flex-box add-new">
                                            <img src="{{voyager_asset('icon/par.svg')}}">
                                            <p>
                                                {{__('voyager::hotdesk.content_search_list')}}
                                            </p>

                                        </button>
                                    </div>


                                </div>
                            </form>

                            <script>

                                $(document).ready(function () {
                                    $(".select-items div").click(function () {
                                        console.log($(this))
                                        $(this).parent().addClass("select-hide")
                                    })
                                })
                            </script>


                        @endif
                        @can('add', app($dataType->model_name))

                            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="flex-box add-new">
                                <img src="{{voyager_asset('icon/par.svg')}}">
                                <p>
                                    {{ __('voyager::generic.add_new') }}
                                </p>

                            </a>
                        @endcan
                    </div>
                    <div class="nav-button-row">
                        <div class="select-all nav-button">
                            {{--
                                                    <ul>
                                                        <li>
                                                            <a>
                                                                همه
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a>
                                                                در انتظار تایید
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a>
                                                                جدید
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a>
                                                                رد شده ها
                                                            </a>
                                                        </li>
                                                    </ul>
                            --}}
                            <div class="check-box">
                                <input class="my-checkbox" type="checkbox" id="select-all">
                                <label for="select-all">
                                    <img src="{{ voyager_asset('icon/white-checkbox.svg') }}">
                                </label>
                            </div>
                            {{--                        <img class="arrow" src="{{ voyager_asset('icon/black-arrow.svg')}}">--}}
                        </div>
                        <a class="hide view-all nav-button" href="{{ request()->url() }}">
                            <img src="{{ voyager_asset('icon/viewall.svg') }}">
                            {{ __('voyager::generic.show_all') }}
                        </a>
                        @if($usesSoftDeletes == true)
                            <a class="hide trash nav-button"
                               href="{{ request()->fullUrlWithQuery(["showSoftDeleted" => true]) }}">
                                <img src="{{ voyager_asset('icon/black-trash.svg') }}">
                                {{ __('voyager::generic.trash') }}
                            </a>
                        @endif
                        {{-- <a class="hide more-item">
                             <img src="{{ voyager_asset('icon/more.svg') }}">
                         </a>--}}
                        <a class="red-item" data-toggle="modal" data-target="#delete_modal1">
                            <img src="{{ voyager_asset('icon/red-trash.svg') }}">
                            <span>
{{ __('voyager::generic.bulk_delete') }} {{ strtolower($dataType->display_name_singular) }}
                        </span>
                        </a>
                        {{--  <a class="red-item">
                              <img src="{{ voyager_asset('icon/requests.svg') }}">
                              تایید درخواست ها
                          </a>
                          <a class="red-item">
                              <img src="{{ voyager_asset('icon/requests.svg') }}">
                              رد درخواست ها
                          </a>--}}
                    </div>
                </nav>
                <div class="more-row flex-box">
                    <div class=" more-item">
                        <ul>
                            <li>
                                <a href="{{ request()->fullUrlWithQuery(["perPage" => 10]) }}">
                                    10
                                    {{ __('voyager::generic.per_page') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ request()->fullUrlWithQuery(["perPage" => 50]) }}">
                                    50
                                    {{ __('voyager::generic.per_page') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ request()->fullUrlWithQuery(["perPage" => 100]) }}">

                                    100
                                    {{ __('voyager::generic.per_page') }}
                                </a>
                            </li>
                        </ul>
                        <img src="{{ voyager_asset('icon/more.svg') }}">
                    </div>
                    <p>

                        {{ __('voyager::generic.page') }}

                        @if(Auth::user()->Locale =='fa')
                            {{ fa_number($dataTypeContent->currentPage()) }}
                        @else
                            {{ ($dataTypeContent->currentPage()) }}

                        @endif

                        {{ __('voyager::generic.from') }}

                        @if(Auth::user()->Locale =='fa')
                            {{ fa_number(ceil($dataTypeContent->total() / $dataTypeContent->perPage())) }}
                        @else
                            {{ (ceil($dataTypeContent->total() / $dataTypeContent->perPage())) }}

                        @endif
                    </p>
                </div>

            </div>
            <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <table>
                            <thead>
                            <tr class="tr">
                                <th class="child3">
                                </th>
                                @foreach($dataType->browseRows as $row)

                                    <th class="child1 text-center">
                                        @if ($isServerSide)
                                            <div class="flex-box list-filter">

                                                <a href="{{ $row->sortByUrl($orderBy, $sortOrder) }}"
                                                   style="color: inherit !important;">
                                                    @endif
                                                    {{ $row->getTranslatedAttribute('display_name') }}
                                                    @if ($isServerSide)
                                                        @if ($row->isCurrentSortField($orderBy))
                                                            @if ($sortOrder == 'asc')
                                                                <i class="voyager-angle-up pull-right"></i>
                                                            @else
                                                                <i class="voyager-angle-down pull-right"></i>
                                                            @endif
                                                        @endif
                                                </a>
                                            </div>

                                        @endif

                                    </th>
                                @endforeach
                                <th class="child2 text-center">
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dataTypeContent as $data)
                                <tr class="tr">
                                    @if($showCheckboxColumn)
                                        <td class="child3">
                                            <div class="check-box">
                                                <input id="checkbox_{{ $data->getKey() }}" type="checkbox"
                                                       value="{{ $data->getKey() }}"/>
                                                <label for="checkbox_{{ $data->getKey() }}">
                                                    <img src="{{voyager_asset('icon/white-checkbox.svg')}}">

                                                </label>
                                            </div>
                                        </td>
                                    @endif
                                    @foreach($dataType->browseRows as $row)
                                        @php
                                            if ($data->{$row->field.'_browse'}) {
                                                $data->{$row->field} = $data->{$row->field.'_browse'};
                                            }
                                        @endphp
                                        <td data-column="{{ $row->getTranslatedAttribute('display_name') }}"
                                            class="child1 text-center">
                                            @if (isset($row->details->view))
                                                @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $data->{$row->field}, 'action' => 'browse', 'view' => 'browse', 'options' => $row->details])
                                            @elseif($row->type == 'image')
                                                <div class="image-box" style="margin: auto;">
                                                    @if( $data->{$row->field})
                                                        <img
                                                            src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $data->{$row->field} ) }}
                                                            @else{{ $data->{$row->field} }}@endif">
                                                    @else
                                                        <img src="{{voyager_asset('icon/Mask.svg')}}">
                                                    @endif
                                                </div>
                                            @elseif($row->type == 'relationship')
                                                @include('voyager::formfields.relationship', ['view' => 'browse','options' => $row->details])
                                            @elseif($row->type == 'select_multiple')
                                                @if(property_exists($row->details, 'relationship'))

                                                    @foreach($data->{$row->field} as $item)
                                                        {{ $item->{$row->field} }}
                                                    @endforeach

                                                @elseif(property_exists($row->details, 'options'))
                                                    @if (!empty(json_decode($data->{$row->field})))
                                                        @foreach(json_decode($data->{$row->field}) as $item)
                                                            @if (@$row->details->options->{$item})
                                                                {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        {{ __('voyager::generic.none') }}
                                                    @endif
                                                @endif

                                            @elseif($row->type == 'multiple_checkbox' && property_exists($row->details, 'options'))
                                                @if (@count(json_decode($data->{$row->field})) > 0)
                                                    @foreach(json_decode($data->{$row->field}) as $item)
                                                        @if (@$row->details->options->{$item})
                                                            {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                        @endif
                                                    @endforeach
                                                @else
                                                    {{ __('voyager::generic.none') }}
                                                @endif

                                            @elseif(($row->type == 'select_dropdown' || $row->type == 'radio_btn') && property_exists($row->details, 'options'))

                                                {!! $row->details->options->{$data->{$row->field}} ?? '' !!}

                                            @elseif($row->type == 'date' || $row->type == 'timestamp')
                                                @if ( property_exists($row->details, 'format') && !is_null($data->{$row->field}) )
                                                    @if(Auth::user()->Locale =='fa')
                                                        {{ \Morilog\Jalali\Jalalian::forge($data->{$row->field})->format($row->details->format) }}
                                                    @else
                                                        {{ \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($row->details->format) }}
                                                    @endif
                                                @else
                                                    @if(Auth::user()->Locale =='fa')
                                                        {{ \Morilog\Jalali\Jalalian::forge($data->{$row->field})->format('Y-m-d H:i:s') }}
                                                    @else
                                                        {{ $data->{$row->field} }}
                                                    @endif

                                                @endif
                                            @elseif($row->type == 'checkbox')
                                                @if(property_exists($row->details, 'on') && property_exists($row->details, 'off'))
                                                    @if($data->{$row->field})
                                                        <div class="green-2 Condition">
                                                            {{ $row->details->on }}
                                                        </div>
                                                    @else
                                                        <div class="red Condition">
                                                            {{ $row->details->off }}
                                                        </div>
                                                    @endif
                                                @else
                                                    {{ $data->{$row->field} }}
                                                @endif
                                            @elseif($row->type == 'color')
                                                <span class="badge badge-lg"
                                                      style="background-color: {{ $data->{$row->field} }}">{{ $data->{$row->field} }}</span>
                                            @elseif($row->type == 'text')
                                                @include('voyager::multilingual.input-hidden-bread-browse')
                                                <div>{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                            @elseif($row->type == 'text_area')
                                                @include('voyager::multilingual.input-hidden-bread-browse')
                                                <div>{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                            @elseif($row->type == 'file' && !empty($data->{$row->field}) )
                                                @include('voyager::multilingual.input-hidden-bread-browse')
                                                @if(json_decode($data->{$row->field}) !== null)
                                                    @foreach(json_decode($data->{$row->field}) as $file)
                                                        <div class="acsses">

                                                            <a style="color: black"
                                                               href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}"
                                                               target="_blank">
                                                                download

                                                            </a>
                                                        </div>

                                                    @endforeach
                                                @else
                                                    <div class="acsses">
                                                        <a style="color: black"
                                                           href="{{ Storage::disk(config('voyager.storage.disk'))->url($data->{$row->field}) }}"
                                                           target="_blank">
                                                            Download
                                                        </a>
                                                    </div>
                                                @endif
                                            @elseif($row->type == 'rich_text_box')
                                                @include('voyager::multilingual.input-hidden-bread-browse')
                                                <div>{{ mb_strlen( strip_tags($data->{$row->field}, '<b><i><u>') ) > 200 ? mb_substr(strip_tags($data->{$row->field}, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($data->{$row->field}, '<b><i><u>') }}</div>
                                            @elseif($row->type == 'coordinates')
                                                @include('voyager::partials.coordinates-static-image')
                                            @elseif($row->type == 'multiple_images')
                                                @php $images = json_decode($data->{$row->field}); @endphp
                                                @if($images)
                                                    @php $images = array_slice($images, 0, 3); @endphp
                                                    @foreach($images as $image)
                                                        <img
                                                            src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif"
                                                            style="width:50px">
                                                    @endforeach
                                                @endif
                                            @elseif($row->type == 'media_picker')
                                                @php
                                                    if (is_array($data->{$row->field})) {
                                                        $files = $data->{$row->field};
                                                    } else {
                                                        $files = json_decode($data->{$row->field});
                                                    }
                                                @endphp
                                                @if ($files)
                                                    @if (property_exists($row->details, 'show_as_images') && $row->details->show_as_images)
                                                        @foreach (array_slice($files, 0, 3) as $file)
                                                            <img
                                                                src="@if( !filter_var($file, FILTER_VALIDATE_URL)){{ Voyager::image( $file ) }}@else{{ $file }}@endif"
                                                                style="width:50px">
                                                        @endforeach
                                                    @else
                                                        <ul>
                                                            @foreach (array_slice($files, 0, 3) as $file)
                                                                <li>{{ $file }}</li>
                                                            @endforeach
                                                        </ul>
                                                    @endif
                                                    @if (count($files) > 3)
                                                        {{ __('voyager::media.files_more', ['count' => (count($files) - 3)]) }}
                                                    @endif
                                                @elseif (is_array($files) && count($files) == 0)
                                                    {{ trans_choice('voyager::media.files', 0) }}
                                                @elseif ($data->{$row->field} != '')
                                                    @if (property_exists($row->details, 'show_as_images') && $row->details->show_as_images)
                                                        <img
                                                            src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif"
                                                            style="width:50px">
                                                    @else
                                                        {{ $data->{$row->field} }}
                                                    @endif
                                                @else
                                                    {{ trans_choice('voyager::media.files', 0) }}
                                                @endif
                                            @else
                                                @include('voyager::multilingual.input-hidden-bread-browse')
                                                <span>{{ $data->{$row->field} }}</span>
                                            @endif
                                        </td>
                                    @endforeach
                                    <td class="child2 text-center">
                                        <div class="flex-box button-row">
                                            @foreach(array_reverse($actions) as $action)
                                                @if (!method_exists($action, 'massAction'))
                                                    @include('voyager::bread.partials.actions', ['action' => $action])
                                                @endif
                                            @endforeach
                                        </div>
                                    </td>
                                    {{-- <td class="no-sort no-click" id="bread-actions">

                                     </td>--}}
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>

                    @if ($isServerSide)
                        {{ $dataTypeContent->appends([
                            's' => $search->value,
                            'filter' => $search->filter,
                            'key' => $search->key,
                            'showSoftDeleted' => $showSoftDeleted,
                            "perPage" => request()->perPage ?? 10
                        ])->links() }}
                    @endif


                </div>
            </div>

        @else
            <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                <nav class="navigation">
                    <div class="right-top-nav flex-box">

                        @if ($isServerSide)
                            <form class="web-item" method="get">
                                <img class="arrow" src="{{voyager_asset('icon/arrow.svg')}}">
                                <img class="search" src="{{voyager_asset('icon/search.svg')}}">
                                <div class="search-box">
                                    <input class="search-input" type="text"
                                           placeholder="{{ __('voyager::generic.search') }}" name="s"
                                           value="{{ $search->value }}">
                                    <div class="search-details">
                                        <div class="row">
                                            <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                                <div class=" input-row ">
                                                    <div class="dropdown custom-selects">

                                                        <select class="select-items " id="search_key" name="key"
                                                                data-placeholder="{{__('voyager::hotdesk.content_search_key')}}">
                                                            <option>
                                                                {{ __('voyager::generic.none') }}
                                                            </option>
                                                            @foreach($searchNames as $key => $name)
                                                                <option value="{{ $key }}"
                                                                        @if($search->key == $key || (empty($search->key) && $key == $defaultSearchKey)) selected @endif>{{ $name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                                <div class=" input-row ">
                                                    <div class="dropdown custom-selects">
                                                        <select class="select-items " id="filter" name="filter"
                                                                data-placeholder="{{__('voyager::hotdesk.content_search_filter')}}"
                                                                data-minimum-results-for-search="Infinity">
                                                            <option>
                                                                {{ __('voyager::generic.none') }}
                                                            </option>
                                                            <option value="contains"
                                                                    @if($search->filter == "contains") selected @endif>
                                                                contains
                                                            </option>
                                                            <option value="equals"
                                                                    @if($search->filter == "equals") selected @endif>=
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                    </div>
                                    <div class="search-details-bottom-item">
                                        <button class="flex-box add-new">
                                            <img src="{{voyager_asset('icon/par.svg')}}">
                                            <p>
                                                {{__('voyager::hotdesk.content_search_list')}}
                                            </p>

                                        </button>
                                    </div>


                                </div>
                            </form>

                            <script>

                                $(document).ready(function () {
                                    $(".select-items div").click(function () {
                                        console.log($(this))
                                        $(this).parent().addClass("select-hide")
                                    })
                                })
                            </script>


                        @endif
                        @can('add', app($dataType->model_name))

                            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="flex-box add-new">
                                <img src="{{voyager_asset('icon/par.svg')}}">
                                <p>
                                    {{ __('voyager::generic.add_new') }}
                                </p>

                            </a>
                        @endcan
                    </div>
                </nav>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12" style="margin-top: 70px;">
                <div class="center-div">
                    <h4 class="bold-font text-center">
                        {{ __('voyager::generic.nothing_found') }}
                    </h4>
                    @can('add', app($dataType->model_name))

                        <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="flex-box add-new"
                           style="margin: auto; width: 170px">
                            <img src="{{voyager_asset('icon/par.svg')}}">
                            <p>
                                {{ __('voyager::generic.add_new') }}
                            </p>

                        </a>
                    @endcan
                </div>
            </div>

        @endif

    </div>

    <div class="modal modal-danger fade show" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="title">
                        {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}
                    </h5>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="flex-box add-new delete-confirm" style="font-size: 18px">
                            {{ __('voyager::generic.delete_confirm') }}
                        </button>

                    </form>
                    <button type="button" class="menu-items close" style="font-size: 18px" data-dismiss="modal">
                        {{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div class="modal modal-danger fade show" tabindex="-1" id="delete_modal1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="title">
                        {{ __('voyager::generic.delete_question') }}
                        {{ strtolower($dataType->display_name_singular) }}
                    </h5>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form1" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input class="deleteIdNo" name="ids" type="hidden" id="bulk_delete_input"/>
                        <button type="submit" class="flex-box add-new delete-confirm" style="font-size: 18px">
                            {{ __('voyager::generic.delete_confirm') }}
                        </button>
                    </form>
                    <button type="button" class="menu-items close" style="font-size: 18px" data-dismiss="modal">
                        {{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection


@section('javascript')



    <script>
        $(".account-number").click(function () {
            if ($(this).attr('title') == '{{ __('voyager::generic.bulk_delete') }}') {
                var idModal = "/" + $(this).attr("data-id");
                $("#delete_form").attr('action', window.location.href + idModal)
            }
        });


        $('#upload-file-btn').on('change', function (e) {
            var fileName = e.target.files[0].name;

            $(".upload-file-row2").append("<div class=\"acsses  files\">" +
                "<div class=\"flex-box\">" +
                "<img class='close-tag' src=\"assets/icon/clos.svg\">"
                + fileName +
                "</div>" +
                "<div class=\"flex-box upload-prosses\">" +
                "<p>Uploading ..</p>" +
                "<p class=\"blue-text\">10 ٪</p>" +
                "</div>" +
                "</div>");
            $('.close-tag').click(function () {
                $(this).parent().parent().fadeOut();
            });

        });
        $('.delet-file').click(function () {
            $('.upload-file-row2 .files').fadeOut();
        });


        $('#upload-file').on('change', function () {
            var filename = $(this).val().replace(/.*(\/|\\)/, '');
            $(".upload-file-row").append("<div class=\"acsses  files\">" +
                "<div class=\"flex-box\">" +
                "<img class='close-tag' src=\"assets/icon/clos.svg\">"
                + filename +
                "</div>" +
                "<div class=\"flex-box upload-prosses\">" +
                "<p>Uploading ..</p>" +
                "<p class=\"blue-text\">10 ٪</p>" +
                "</div>" +
                "</div>");
            $('.close-tag').click(function () {
                $(this).parent().parent().fadeOut();
            });
        });

    </script>

    <!-- DataTables -->
    <script>

        var table = {
            name: '',
            rows: []
        };

        new Vue({
            el: '#table_info',
            data: {
                table: table,
            },
        });

        $(function () {

            // Setup Delete BREAD
            //
            $('table .actions').on('click', '.delete', function (e) {
                id = $(this).data('id');
                name = $(this).data('name');

                $('#delete_builder_name').text(name);
                $('#delete_builder_form')[0].action = '{{ route('voyager.bread.delete', ['__id']) }}'.replace('__id', id);
                $('#delete_builder_modal').modal('show');
            });

            // Setup Show Table Info
            //
            $('.database-tables').on('click', '.desctable', function (e) {
                e.preventDefault();
                href = $(this).attr('href');
                table.name = $(this).data('name');
                table.rows = [];
                $.get(href, function (data) {
                    $.each(data, function (key, val) {
                        table.rows.push({
                            Field: val.field,
                            Type: val.type,
                            Null: val.null,
                            Key: val.key,
                            Default: val.default,
                            Extra: val.extra
                        });
                        $('#table_info').modal('show');
                    });
                });
            });
        });
    </script>

    <script>
        $("#select-all").click(function () {
            $("input[type=checkbox]").prop('checked', $(this).prop('checked'));

        });

        $(".my-checkbox:checkbox").click(function () {
            if ($('.my-checkbox:checkbox').is(":checked")) {
                $(' .hide').css("display", "none")
                $(' .red-item').css("display", "inline-flex")

                var checkBoxArray = $(".check-box input");

                var result = checkBoxArray.filter(function (elem) {
                    return elem != 0;
                });

                var newVal = '';

                for (var i = 0; i < result.length; i++) {

                    if (i == 0) {
                        newVal += ($(result[i]).val())
                    } else {
                        newVal += ("," + $(result[i]).val())
                    }

                }

                $(".deleteIdNo").val(newVal)
                $("#delete_form1").attr("action", window.location.href + "/0")


                // console.log($(".deleteIdNo").val().split(","))

            } else {
                $(' .hide').css("display", "inline-flex")
                $(' .red-item').css("display", "none")
                $(".deleteIdNo").val('')


            }
        });
        $(".deleteIdNo").val("")

        $(".check-box input:checkbox").click(function (e) {
            var oldVal = $(".deleteIdNo").val();


            if ($(this).is(":checked")) {


                if (oldVal.trim() == "") {

                    $(' .hide').css("display", "none")
                    $(' .red-item').css("display", "inline-flex");
                    $("#delete_form1").attr("action", window.location.href + "/0")
                    $(".deleteIdNo").val(oldVal + e.target.value);
                } else {
                    $(' .red-item:eq(0)').children("span").text("{{ __('voyager::generic.bulk_delete') }} {{ strtolower($dataType->display_name_plural) }}");
                    $(".deleteIdNo").val(oldVal + "," + e.target.value);
                }


            } else {
                if ($(".deleteIdNo").val().length == 1) {//before any edit on value of input
                    $(' .hide').css("display", "inline-flex")
                    $(' .red-item').css("display", "none")
                } else if ($(".deleteIdNo").val().length == 3) {
                    $(' .red-item:eq(0)').children("span").text("{{ __('voyager::generic.bulk_delete') }} {{ strtolower($dataType->display_name_singular) }}");
                }

                var NoArray = oldVal.split(",");

                var result = NoArray.filter(function (elem) {
                    return elem != e.target.value;
                })
                var newVal = '';

                for (var i = 0; i < result.length; i++) {
                    if (i == 0) {
                        newVal += (result[i])
                    } else {
                        newVal += ("," + result[i])
                    }

                }

                $(".deleteIdNo").val(newVal);


            }
        });


        $(".select-all").click(function (e) {
            $(this).children("ul").toggle();
            $(this).toggleClass("active")
            e.stopPropagation();


        });

        $(".select-all ul").click(function (e) {
            e.stopPropagation();
        });

        $(document).click(function () {
            $(".select-all ul").hide();
            $(".select-all").removeClass("active")
        });


        $(".more-row .more-item").click(function (e) {
            $(this).children("ul").toggle();
            $(this).toggleClass("active")
            e.stopPropagation();


        });

        $(".more-row .more-item ul").click(function (e) {
            e.stopPropagation();
        });

        $(document).click(function () {
            $(".more-row .more-item ul").hide();
            $(".more-row .more-item").removeClass("active")
        });
    </script>
    <script>

        $(".list-filter").click(function (e) {
            if ($(this).hasClass('active')) {
                $(".list-filter ul").hide();
                $(" .list-filter ").removeClass("active")
            } else {
                $(".list-filter ul").hide();
                $(" .list-filter ").removeClass("active")
                $(this).addClass("active")
                $(this).children("ul").toggle();
            }


            e.stopPropagation();


        });

        $(".list-filter ul").click(function (e) {
            e.stopPropagation();
        });

        $(document).click(function () {
            $(".list-filter ul").hide();
            $(" .list-filter ").removeClass("active")
        });
    </script>

@endsection
