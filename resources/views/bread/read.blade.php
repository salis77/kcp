@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.$dataType->display_name_singular)

@section('content')
    <div class="row">
        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
            <nav class="navigation">
                <div class="right-top-nav flex-box">
                    <a href="{{ url()->previous() }}" class="flex-box back">
                        <img src="{{ voyager_asset('icon/back.svg') }}">
                        {{ __('voyager::generic.return_to_list') }}

                    </a>
                </div>
            </nav>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12">
                    <div class="row">

                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="box">
                                <form action="#" class="form">
                                    <div class="row">

                                        @foreach($dataType->readRows as $row)
                                            @php
                                                if ($dataTypeContent->{$row->field.'_read'}) {
                                                    $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_read'};
                                                }
                                            @endphp
                                            <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">

                                                <div class="input-row-value">
                                                    <label>{{ $row->getTranslatedAttribute('display_name') }}</label>
                                                    <div class="same-input-div">

                                                        @if (isset($row->details->view))
                                                            @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => 'read', 'view' => 'read', 'options' => $row->details])
                                                        @elseif($row->type == "image")
                                                                <img width="100"
                                                                     src="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">

                                                        @elseif($row->type == 'multiple_images')
                                                            @if(json_decode($dataTypeContent->{$row->field}))
                                                                @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
                                                                    <img class="img-responsive"
                                                                         src="{{ filter_var($file, FILTER_VALIDATE_URL) ? $file : Voyager::image($file) }}">
                                                                @endforeach
                                                            @else
                                                                <img class="img-responsive"
                                                                     src="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">
                                                            @endif
                                                        @elseif($row->type == 'relationship')
                                                            @include('voyager::formfields.relationship', ['view' => 'read', 'options' => $row->details])
                                                        @elseif($row->type == 'select_dropdown' && property_exists($row->details, 'options') &&
                                                                !empty($row->details->options->{$dataTypeContent->{$row->field}})
                                                        )
                                                            <?php echo $row->details->options->{$dataTypeContent->{$row->field}};?>
                                                        @elseif($row->type == 'select_multiple')
                                                            @if(property_exists($row->details, 'relationship'))

                                                                @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                                                                    {{ $item->{$row->field}  }}
                                                                @endforeach

                                                            @elseif(property_exists($row->details, 'options'))
                                                                @if (!empty(json_decode($dataTypeContent->{$row->field})))
                                                                    @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                                                                        @if (@$row->details->options->{$item})
                                                                            {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    {{ __('voyager::generic.none') }}
                                                                @endif
                                                            @endif
                                                        @elseif($row->type == 'date' || $row->type == 'timestamp')
                                                            @if ( property_exists($row->details, 'format') && !is_null($dataTypeContent->{$row->field}) )
                                                                @if(Auth::user()->Locale =='fa')
                                                                    {{ \Morilog\Jalali\Jalalian::forge($dataTypeContent->{$row->field})->format($row->details->format) }}
                                                                @else
                                                                    {{ \Carbon\Carbon::parse($dataTypeContent->{$row->field})->formatLocalized($row->details->format) }}
                                                                @endif
                                                            @else
                                                                @if(Auth::user()->Locale =='fa')
                                                                    {{ \Morilog\Jalali\Jalalian::forge($dataTypeContent->{$row->field})->format('Y-m-d H:i:s') }}
                                                                @else
                                                                    {{ $dataTypeContent->{$row->field} }}
                                                                @endif
                                                            @endif
                                                        @elseif($row->type == 'checkbox')
                                                            @if(property_exists($row->details, 'on') && property_exists($row->details, 'off'))
                                                                @if($dataTypeContent->{$row->field})
                                                                    <span style="margin:0 "
                                                                          class="green-2 Condition">{{ $row->details->on }}</span>
                                                                @else
                                                                    <span style="margin:0 "
                                                                          class="red Condition">{{ $row->details->off }}</span>
                                                                @endif
                                                            @else
                                                                {{ $dataTypeContent->{$row->field} }}
                                                            @endif
                                                        @elseif($row->type == 'color')
                                                            <span class="badge badge-lg"
                                                                  style="background-color: {{ $dataTypeContent->{$row->field} }}">{{ $dataTypeContent->{$row->field} }}</span>
                                                        @elseif($row->type == 'coordinates')
                                                            @include('voyager::partials.coordinates')
                                                        @elseif($row->type == 'rich_text_box')
                                                            @include('voyager::multilingual.input-hidden-bread-read')
                                                            {!! $dataTypeContent->{$row->field} !!}
                                                        @elseif($row->type == 'file')
                                                            @if(json_decode($dataTypeContent->{$row->field}))
                                                                @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
                                                                    <div style="margin: 0" class="account-number">
                                                                        <a style="color: black"
                                                                           href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}">
                                                                            Download
                                                                        </a>
                                                                    </div>
                                                                @endforeach
                                                            @else
                                                                <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($row->field) ?: '' }}">
                                                                    {{ __('voyager::generic.download') }}
                                                                </a>
                                                            @endif
                                                        @else
                                                            @include('voyager::multilingual.input-hidden-bread-read')
                                                            @if(is_array($dataTypeContent->{$row->field}))
                                                                {!! json_printer($dataTypeContent->{$row->field}) !!}
                                                            @else
                                                                {{ is_array($dataTypeContent->{$row->field}) ?  : $dataTypeContent->{$row->field} }}
                                                            @endif
                                                        @endif
                                                    </div><!-- panel-body -->
                                                </div>
                                            </div>


                                        @endforeach
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="row">
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <h5 class="title">
                                {{ __('voyager::generic.guide') }}
                            </h5>
                        </div>
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <p class="Description">
                                {{ __('voyager::generic.lorem_ipsum') }}

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><i
                            class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}
                        ?</h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('voyager.'.$dataType->slug.'.index') }}" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                               value="{{ __('voyager::generic.delete_confirm') }} {{ strtolower($dataType->display_name_singular) }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('javascript')
    @if ($isModelTranslatable)
        <script>
            $(document).ready(function () {
                $('.side-body').multilingual();
            });
        </script>
    @endif
    <script>
        var deleteFormAction;
        $('.delete').on('click', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) {
                // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');

            $('#delete_modal').modal('show');
        });

    </script>
@stop
