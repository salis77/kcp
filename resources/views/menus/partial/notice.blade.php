@if(config('voyager.show_dev_tips'))
    <div class="padding-item col-lg-12 col-md-12 col-sm-12">
        <div class="box">
            <div class=" flex-box edit-massage">
                <div class="yellow flex-box  image-box">
                    <img src="{{voyager_asset('icon/Help.svg')}}">
                </div>
                <p>
                    {{ trans_choice('voyager::menu_builder.usage_hint', !empty($menu) ? 0 : 1) }}
                    <a class="acsses">
                        menu('{{ !empty($menu) ? $menu->name : 'name' }}')
                    </a>
                </p>

            </div>
        </div>
    </div>
@endif

