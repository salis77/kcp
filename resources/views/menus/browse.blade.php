@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->getTranslatedAttribute('display_name_plural'))

@section('page_header')
    <div class="action">
        @can('add',app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-blue add">
                {{ __('voyager::generic.add_new') }}
            </a>
        @endcan
    </div>
@stop

@section('content')

    <div class="row">
        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
            <nav class="navigation">
                <div class="right-top-nav flex-box">
                    @can('add',app($dataType->model_name))

                        <a class="flex-box add-new" href="{{ route('voyager.'.$dataType->slug.'.create') }}">
                            <img src="{{voyager_asset('icon/par.svg')}}">
                            <p>
                                {{ __('voyager::generic.add_new') }}
                            </p>
                        </a>
                    @endcan

                </div>


            </nav>
        </div>
        @include('voyager::menus.partial.notice')

        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <table>
                        <thead>
                        <tr class="tr">

                            @foreach($dataType->browseRows as $row)
                                <th class="child1">
                                    <div class="flex-box list-filter">
                                        {{ $row->display_name }}
                                    </div>

                                </th>
                            @endforeach

                           {{-- <th class="child2 " style="text-align: left">
                                {{ __('voyager::generic.actions') }}
                            </th>--}}


                        </tr>
                        </thead>
                        <tbody>
                        @foreach($dataTypeContent as $data)


                            <tr class="tr">
                                <td >
                                    <div class="flex-box">
                                        @if($row->type == 'image')

                                            <div class="image-box">
                                                <img
                                                    src="@if( strpos($data->{$row->field}, 'http://') === false && strpos($data->{$row->field}, 'https://') === false){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif">
                                            </div>
                                        @else
                                            <div>
                                                <h6>
                                                    {{ $data->{$row->field} }}
                                                </h6>

                                            </div>
                                        @endif
                                    </div>
                                </td>
                                <td class="child2 text-center">
                                    <div class="flex-box button-row">


                                        @can('delete', $data)
                                            <div class="btn btn-sm  pull-right delete account-number"
                                                 data-id="{{ $data->{$data->getKeyName()} }}">
                                                <img src="{{ voyager_asset('icon/trash.svg') }}">

                                            </div>
                                        @endcan
                                               @can('edit', $data)
                                                   <a href="{{ route('voyager.'.$dataType->slug.'.edit', $data->{$data->getKeyName()}) }}"
                                                      class="account-number ">
                                                       <img src="{{ voyager_asset('icon/Pen.svg') }}">


                                                   </a>
                                               @endcan
                                               @can('edit', $data)
                                                   <a class="account-number "
                                                      href="{{ route('voyager.'.$dataType->slug.'.builder', $data->{$data->getKeyName()}) }}">
                                                       <img src="{{ voyager_asset('icon/eye.svg') }}">
                                                   </a>
                                               @endcan

                                    </div>
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="title">
                        {{ __('voyager::generic.delete_question') }}
                        {{ $dataType->getTranslatedAttribute('display_name_singular') }}
                        ?
                    </h5>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <button type="submit"  class="flex-box add-new delete-confirm" style="font-size: 18px">
                            {{ __('voyager::generic.delete_this_confirm') }}
                            {{ $dataType->getTranslatedAttribute('display_name_singular') }}
                        </button>
                    </form>
                    <button type="button" class="menu-items close" style="font-size: 18px" data-dismiss="modal">
                        {{ __('voyager::generic.cancel') }}
                    </button>
                </div>
            </div>
        </div>
    </div>

@stop

@section('javascript')
    <!-- DataTables -->
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable({
                    "order": [],
                    "language": {!! json_encode(__('voyager::datatable'), true) !!},
                    "columnDefs": [{"targets": -1, "searchable": false, "orderable": false}],
                    "dom": '<"filters"f><"datatableserv"rt><"bottom"pli><"clear">',
                @if(config('voyager::dashboard.data_tables.responsive')) , responsive
        :
            true @endif
        })
            ;
        });
        $(document).on('preInit.dt', function (settings, json) {
            $('div.dataTables_length select').select2();
            $('div.dataTables_length').addClass('form-group').addClass('select');
            $('div.dataTables_length label').addClass('select');
            $('.dataTables_wrapper .dataTables_filter').addClass('form-group').addClass('search');
            $('.dataTables_wrapper .dataTables_filter input[type=search]').attr('placeholder', '{{__('voyager::hotdesk.bread_search')}}');
            $('.table-responsive').addClass('notserverside');
        });

        $(".pull-right.delete").click(function (){

            $('#delete_modal').modal('show');
            $('#delete_modal').addClass('show');

        })

        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__menu']) }}'.replace('__menu', $(this).data('id'));

            $('#delete_modal').modal('show');
        });



    </script>
@stop
