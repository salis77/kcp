@php

    if (Voyager::translatable($items)) {
        $items = $items->load('translations');
    }
@endphp


@foreach($items as $menu_item)

    @php

        if (Voyager::translatable($menu_item)) {
            $menu_item = $menu_item->translate($options->locale);
        }
        if($menu_item->route!="" && !Auth::user()->hasPermission(str_replace('-','_',"browse_".str_replace(['voyager.','.index'],'',$menu_item->route))) && $menu_item->route!="voyager.dashboard")
          continue;
        $links = [url($menu_item->link())];
        $permission = 0;
    @endphp

    @foreach($menu_item->children as $item)
        @php
            if($item->route!="" && !Auth::user()->hasPermission(str_replace('-','_',"browse_".str_replace(['voyager.','.index'],'',$item->route))) && $item->route!="voyager.dashboard")
              continue;
            $links[] = url($item->link());
            $permission++;
        @endphp
    @endforeach

    @if($permission == 0 && count($menu_item->children))
        @php
            continue;
        @endphp
    @endif

    <li class="@if(in_array(url()->current(),$links)) active @endif @if(count($menu_item->children) > 0) sub-menu @endif">

        <a class="flex-box nav-item" @if(count($menu_item->children) > 0)
           @else href='{{ asset($menu_item->link()) }}' @endif>

            <i class="{{ $menu_item->icon_class ?? 'voyager-dot'}}" style="font-size: 20px;height: 25px;margin-right: 12px;margin-left: 12px;"></i>


            {{ $menu_item->title }}

            @if(false)
                <div class="massage-number">
                    +۹۹
                </div>
            @endif

            @if(count($menu_item->children) > 0)
                <svg class="arrow" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path d="M14 8L10 12L14 16" stroke="#" stroke-width="1.5" stroke-linecap="round"
                          stroke-linejoin="round"/>
                </svg>
            @endif
        </a>

        @if(count($menu_item->children) > 0)
            <ul>
                @foreach($menu_item->children as $item)

                    @php
                        if($item->route!="" && !Auth::user()->hasPermission(str_replace('-','_',"browse_".str_replace(['voyager.','.index'],'',$item->route))) && $item->route!="voyager.dashboard")
                          continue;
                        if (Voyager::translatable($item)) {
                            $item = $item->translate($options->locale);
                        }
                    @endphp

                    <li>
                        <a class="flex-box" id="{{ $item->title }}" href="{{ asset($item->link()) }}" target="{{$item->target}}">
                            <i class="{{ $item->icon_class ?? 'voyager-dot'}}" style="font-size: 20px;height: 25px;@if(app()->getLocale() === "fa") margin-right: -28px;margin-left: 12px; @else  margin-left: -28px;margin-right: 12px; @endif"></i>
                            {{$item->title}}
                        </a>
                    </li>
                @endforeach
            </ul>
        @else

        @endif

        {{--<button type="button" title="{{$menu_item->title}}">
            @if(preg_match('/<svg/', $menu_item->icon_class, $output_array))
                {!!$menu_item->icon_class!!}
            @elseif($menu_item->icon_class)
                <i class="{{$menu_item->icon_class}}"></i>
            @else
                <i class="voyager-dot"></i>
            @endif
        </button>--}}
        {{--<div class="submenu">
            <button type="button" class="closemenu"></button>
            <ul>
                @if(!count($menu_item->children))
                    <li><a href="{{$menu_item->link()}}" target="{{$menu_item->target}}">{{$menu_item->title}}</a>
                    </li>
                @endif
                @foreach($menu_item->children as $item)
                    @php
                        if($item->route!="" && !Auth::user()->hasPermission(str_replace('-','_',"browse_".str_replace(['voyager.','.index'],'',$item->route))) && $item->route!="voyager.dashboard")
                          continue;
                        if (Voyager::translatable($item)) {
                            $item = $item->translate($options->locale);
                        }
                    @endphp
                    <li><a href="{{ $item->link()}}" target="{{$item->target}}">{{$item->title}}</a></li>
                @endforeach
            </ul>
        </div>--}}

    </li>
@endforeach
<script>
    const url = window.location.href;
    const urlOrigin = window.location.origin;
    const urlPathname = window.location.pathname;

    const child_element = $('a[href="' + urlOrigin + urlPathname +'"]');

    child_element.parent().parent().css('display', 'inline-block')
    child_element.parent().parent().css('width', '100%')
    child_element.parent().parent().prev().addClass('nav-item activete')
    // child_element.parent().addClass('nav-item activete')
</script>
