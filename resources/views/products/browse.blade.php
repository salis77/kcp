@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

{{--
@section('page_header')
    <div class="action">
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-blue add">
                {{ __('voyager::generic.add_new') }}
            </a>
        @endcan
        @can('delete', app($dataType->model_name))
            @include('voyager::partials.bulk-delete')
        @endcan
        @can('edit', app($dataType->model_name))
            @if(isset($dataType->order_column) && isset($dataType->order_display_column))
                <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-blue">
                    <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
                </a>
            @endif
        @endcan
        @can('delete', app($dataType->model_name))
            @if($usesSoftDeletes)
                <input type="checkbox" @if ($showSoftDeleted) checked @endif id="show_soft_deletes" data-toggle="toggle"
                       data-on="{{ __('voyager::bread.soft_deletes_off') }}"
                       data-off="{{ __('voyager::bread.soft_deletes_on') }}">
            @endif
        @endcan
        @foreach($actions as $action)
            @if (method_exists($action, 'massAction'))
                @include('voyager::bread.partials.actions', ['action' => $action, 'data' => null])
            @endif
        @endforeach
        @include('voyager::multilingual.language-selector')
    </div>
@stop
--}}

@section('content')

    <div class="row">
        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
            <nav class="navigation">
                <div class="right-top-nav flex-box">

                    @if ($isServerSide)
                        <form class="web-item" method="get">
                            <img class="arrow" src="{{voyager_asset('icon/arrow.svg')}}">
                            <img class="search" src="{{voyager_asset('icon/search.svg')}}">
                            <div class="search-box">
                                <input class="search-input" type="text"
                                       placeholder="{{ __('voyager::generic.search') }}" name="s"
                                       value="{{ $search->value }}">
                                <div class="search-details">
                                    <div class="row">
                                        <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                            <div class=" input-row ">
                                                <div class="dropdown custom-selects">

                                                    <select class="select-items " id="search_key" name="key"
                                                            data-placeholder="{{__('voyager::hotdesk.content_search_key')}}">
                                                        <option>
                                                            {{ __('voyager::generic.none') }}
                                                        </option>
                                                        @foreach($searchNames as $key => $name)
                                                            <option value="{{ $key }}"
                                                                    @if($search->key == $key || (empty($search->key) && $key == $defaultSearchKey)) selected @endif>{{ $name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                            <div class=" input-row ">
                                                <div class="dropdown custom-selects">
                                                    <select class="select-items " id="filter" name="filter"
                                                            data-placeholder="{{__('voyager::hotdesk.content_search_filter')}}"
                                                            data-minimum-results-for-search="Infinity">
                                                        <option>
                                                            {{ __('voyager::generic.none') }}
                                                        </option>
                                                        <option value="contains"
                                                                @if($search->filter == "contains") selected @endif>
                                                            contains
                                                        </option>
                                                        <option value="equals"
                                                                @if($search->filter == "equals") selected @endif>=
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="search-details-bottom-item">
                                    <button class="flex-box add-new">
                                        <img src="{{voyager_asset('icon/par.svg')}}">
                                        <p>
                                            {{__('voyager::hotdesk.content_search_list')}}
                                        </p>

                                    </button>
                                </div>
                            </div>
                        </form>
                    @endif
                    @can('add', app($dataType->model_name))

                        <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="flex-box add-new">
                            <img src="{{voyager_asset('icon/par.svg')}}">
                            <p>
                                {{ __('voyager::generic.add_new') }}
                            </p>

                        </a>
                    @endcan

                </div>
                <div class="nav-button-row">
                    <div class="select-all nav-button">
                        {{--
                                                <ul>
                                                    <li>
                                                        <a>
                                                            همه
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a>
                                                            در انتظار تایید
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a>
                                                            جدید
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a>
                                                            رد شده ها
                                                        </a>
                                                    </li>
                                                </ul>
                        --}}
                        <div class="check-box">
                            <input class="my-checkbox" type="checkbox" id="select-all">
                            <label for="select-all">
                                <img src="{{ voyager_asset('icon/white-checkbox.svg') }}">
                            </label>
                        </div>
                        {{--                        <img class="arrow" src="{{ voyager_asset('icon/black-arrow.svg')}}">--}}
                    </div>
                    <a class="hide view-all nav-button" href="{{ request()->url() }}">
                        <img src="{{ voyager_asset('icon/viewall.svg') }}">
                        {{ __('voyager::generic.show_all') }}

                    </a>
                    @if($usesSoftDeletes == true)
                        <a class="hide trash nav-button"
                           href="{{ request()->fullUrlWithQuery(["showSoftDeleted" => true]) }}">
                            <img src="{{ voyager_asset('icon/black-trash.svg') }}">
                            {{ __('voyager::generic.trash') }}
                        </a>
                    @endif
                    {{-- <a class="hide more-item">
                         <img src="{{ voyager_asset('icon/more.svg') }}">
                     </a>--}}
                    <a class="red-item" data-toggle="modal" data-target="#delete_modal1">
                        <img src="{{ voyager_asset('icon/red-trash.svg') }}">
                        <span>
                            {{ __('voyager::generic.bulk_delete') }} {{ strtolower($dataType->display_name_singular) }}
                        </span>
                    </a>
                    {{--  <a class="red-item">
                          <img src="{{ voyager_asset('icon/requests.svg') }}">
                          تایید درخواست ها
                      </a>
                      <a class="red-item">
                          <img src="{{ voyager_asset('icon/requests.svg') }}">
                          رد درخواست ها
                      </a>--}}
                </div>
            </nav>
            <div class="more-row flex-box">
                <div class=" more-item">
                    <ul>
                        <li>
                            <a href="{{ request()->fullUrlWithQuery(["perPage" => 10]) }}">
                                10
                                {{ __('voyager::generic.per_page') }}
                            </a>
                        </li>
                        <li>
                            <a href="{{ request()->fullUrlWithQuery(["perPage" => 50]) }}">
                                50
                                {{ __('voyager::generic.per_page') }}
                            </a>
                        </li>
                        <li>
                            <a href="{{ request()->fullUrlWithQuery(["perPage" => 100]) }}">
                                100
                                {{ __('voyager::generic.per_page') }}
                            </a>
                        </li>
                    </ul>
                    <img src="{{ voyager_asset('icon/more.svg') }}">
                </div>
                <p>
                    {{ __('voyager::generic.page') }}

                    @if(Auth::user()->Locale =='fa')
                        {{ fa_number($dataTypeContent->currentPage()) }}
                    @else
                        {{ ($dataTypeContent->currentPage()) }}

                    @endif


                    {{ __('voyager::generic.from') }}
                    @if(Auth::user()->Locale =='fa')

                        {{fa_number(ceil($dataTypeContent->total() / $dataTypeContent->perPage())) }}
                    @else
                        {{(ceil($dataTypeContent->total() / $dataTypeContent->perPage())) }}

                    @endif
                </p>
            </div>

        </div>
        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
            <div class="row">


                @foreach($dataTypeContent as $data)
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="box">
                            <div class="row">
                                @php
                                    $rows = $dataType->browseRows;
                                @endphp

                                <div class="col-xl-2 col-lg-4 col-md-4 col-sm-4 col-12">
                                    <div class="flex-box product-imge-row">
                                        <div class="check-box">
                                            <input class="my-checkbox" type="checkbox" id="row1">
                                            <label for="row1">
                                                <img src="{{voyager_asset('icon/white-checkbox.svg')}}">
                                            </label>
                                        </div>

                                        <div>

                                            <div class="image-box">
                                                @if($data->main_image)
                                                    <img src="{{ Voyager::image($data->main_image) }}">
                                                @else
                                                    <img src="{{voyager_asset('icon/Mask.svg')}}">
                                                @endif
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-10 col-lg-8 col-md-8 col-sm-8 col-12">
                                    <div class="product-text">
                                        <div class="flex-box">
                                            <div class="product-text-title">
                                                <h6>
                                                    {{ $data->title }}
                                                </h6>
                                                <div class="date flex-box">
                                                    <img src="{{voyager_asset('icon/calender.svg')}}">
                                                    @if(Auth::user()->Locale =='fa')

                                                        {{ \Morilog\Jalali\Jalalian::forge($data->created_at)->format(' %d , %B , %Y') }}
                                                    @else
                                                        {{ $data->created_at}}
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="flex-box button-row">
                                                @foreach(array_reverse($actions) as $action)
                                                    @if (!method_exists($action, 'massAction'))
                                                        @include('voyager::bread.partials.actions', ['action' => $action])
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                        <p>
                                            {{ $data->description }}
                                        </p>
                                        <div class="row product-text-details">
                                            @if($data->sells)
                                                <div class="padding-item col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6">
                                                    <div class="flex-box">
                                                        <div>
                                                            <p>
                                                                {{ __('voyager::generic.total_sales') }}

                                                            </p>
                                                            <p>
                                                                {{ number_format($data->sells) }}
                                                            </p>
                                                        </div>
                                                        <img src="{{voyager_asset('icon/all-price.svg')}}">
                                                    </div>
                                                </div>
                                            @endif
                                            @if($data->sell_num)
                                                <div class="padding-item col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6">
                                                    <div class="flex-box">
                                                        <div>
                                                            <p>
                                                                {{ __('voyager::generic.sales_number') }}
                                                            </p>
                                                            <p>
                                                                {{ $data->sell_num }}
                                                            </p>
                                                        </div>
                                                        <img src="{{voyager_asset('icon/sale-number.svg')}}">
                                                    </div>
                                                </div>
                                            @endif
                                            @if($data->price)
                                                <div class="padding-item col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6">
                                                    <div class="flex-box">
                                                        <div>
                                                            <p>
                                                                {{ __('voyager::generic.price') }}
                                                            </p>
                                                            <p>
                                                                {{ number_format($data->price) }}
                                                            </p>
                                                        </div>
                                                        <img src="{{voyager_asset('icon/price.svg')}}">
                                                    </div>
                                                </div>
                                            @endif
                                            @if($data->stock)
                                                <div class="padding-item col-xl-2 col-lg-3 col-md-6 col-sm-6 col-6">
                                                    <div class="flex-box">
                                                        <div>
                                                            <p>
                                                                {{ __('voyager::generic.stock') }}

                                                            </p>
                                                            <p>
                                                                {{ $data->stock }}
                                                            </p>
                                                        </div>
                                                        <img src="{{voyager_asset('icon/Inventory.svg')}}">
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach


                @if ($isServerSide)
                    {{ $dataTypeContent->appends([
                        's' => $search->value,
                        'filter' => $search->filter,
                        'key' => $search->key,
                        'showSoftDeleted' => $showSoftDeleted,
                        "perPage" => request()->perPage ?? 10
                    ])->links() }}
                @endif

            </div>
        </div>
    </div>

    <div class="modal modal-danger fade show" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="title">
                        {{ __('voyager::generic.delete_question') }}
                        {{ strtolower($dataType->display_name_singular) }}
                    </h5>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="flex-box add-new delete-confirm" style="font-size: 18px">
                            {{ __('voyager::generic.delete_confirm') }}
                        </button>
                    </form>
                    <button type="button" class="menu-items close" style="font-size: 18px" data-dismiss="modal">
                        {{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div class="modal modal-danger fade show" tabindex="-1" id="delete_modal1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <h5 class="title">
                        {{ __('voyager::generic.delete_question') }}
                        {{ strtolower($dataType->display_name_singular) }}
                    </h5>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form1" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input class="deleteIdNo" name="ids" type="hidden" id="bulk_delete_input"/>
                        <button type="submit" class="flex-box add-new delete-confirm" style="font-size: 18px">
                            {{ __('voyager::generic.delete_confirm') }}
                        </button>
                    </form>
                    <button type="button" class="menu-items close" style="font-size: 18px" data-dismiss="modal">
                        {{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection


@section('javascript')



    <script>
        $(".account-number").click(function () {
            if ($(this).attr('title') == '{{ __('voyager::generic.bulk_delete') }}') {
                var idModal = "/" + $(this).attr("data-id");
                $("#delete_form").attr('action', window.location.href + idModal)
            }
        })
    </script>

    <!-- DataTables -->
    <script>

        var table = {
            name: '',
            rows: []
        };

        new Vue({
            el: '#table_info',
            data: {
                table: table,
            },
        });

        $(function () {

            // Setup Delete BREAD
            //
            $('table .actions').on('click', '.delete', function (e) {
                id = $(this).data('id');
                name = $(this).data('name');

                $('#delete_builder_name').text(name);
                $('#delete_builder_form')[0].action = '{{ route('voyager.bread.delete', ['__id']) }}'.replace('__id', id);
                $('#delete_builder_modal').modal('show');
            });

            // Setup Show Table Info
            //
            $('.database-tables').on('click', '.desctable', function (e) {
                e.preventDefault();
                href = $(this).attr('href');
                table.name = $(this).data('name');
                table.rows = [];
                $.get(href, function (data) {
                    $.each(data, function (key, val) {
                        table.rows.push({
                            Field: val.field,
                            Type: val.type,
                            Null: val.null,
                            Key: val.key,
                            Default: val.default,
                            Extra: val.extra
                        });
                        $('#table_info').modal('show');
                    });
                });
            });
        });
    </script>

    <script>
        $("#select-all").click(function () {
            $("input[type=checkbox]").prop('checked', $(this).prop('checked'));

        });

        $(".my-checkbox:checkbox").click(function () {
            if ($('.my-checkbox:checkbox').is(":checked")) {
                $(' .hide').css("display", "none")
                $(' .red-item').css("display", "inline-flex")

                var checkBoxArray = $(".check-box input");

                var result = checkBoxArray.filter(function (elem) {
                    return elem != 0;
                });

                var newVal = '';

                for (var i = 0; i < result.length; i++) {

                    if (i == 0) {
                        newVal += ($(result[i]).val())
                    } else {
                        newVal += ("," + $(result[i]).val())
                    }

                }

                $(".deleteIdNo").val(newVal)
                $("#delete_form1").attr("action", window.location.href + "/0")


                // console.log($(".deleteIdNo").val().split(","))

            } else {
                $(' .hide').css("display", "inline-flex")
                $(' .red-item').css("display", "none")
                $(".deleteIdNo").val('')


            }
        });
        $(".deleteIdNo").val("")

        $(".check-box input:checkbox").click(function (e) {
            var oldVal = $(".deleteIdNo").val();


            if ($(this).is(":checked")) {


                if (oldVal.trim() == "") {

                    $(' .hide').css("display", "none")
                    $(' .red-item').css("display", "inline-flex");
                    $("#delete_form1").attr("action", window.location.href + "/0")
                    $(".deleteIdNo").val(oldVal + e.target.value);
                } else {
                    $(' .red-item:eq(0)').children("span").text("{{ __('voyager::generic.bulk_delete') }} {{ strtolower($dataType->display_name_plural) }}");
                    $(".deleteIdNo").val(oldVal + "," + e.target.value);
                }


            } else {
                if ($(".deleteIdNo").val().length == 1) {//before any edit on value of input
                    $(' .hide').css("display", "inline-flex")
                    $(' .red-item').css("display", "none")
                } else if ($(".deleteIdNo").val().length == 3) {
                    $(' .red-item:eq(0)').children("span").text("{{ __('voyager::generic.bulk_delete') }} {{ strtolower($dataType->display_name_singular) }}");
                }

                var NoArray = oldVal.split(",");

                var result = NoArray.filter(function (elem) {
                    return elem != e.target.value;
                })
                var newVal = '';

                for (var i = 0; i < result.length; i++) {
                    if (i == 0) {
                        newVal += (result[i])
                    } else {
                        newVal += ("," + result[i])
                    }

                }

                $(".deleteIdNo").val(newVal);


            }
        });


        $(".select-all").click(function (e) {
            $(this).children("ul").toggle();
            $(this).toggleClass("active")
            e.stopPropagation();


        });

        $(".select-all ul").click(function (e) {
            e.stopPropagation();
        });

        $(document).click(function () {
            $(".select-all ul").hide();
            $(".select-all").removeClass("active")
        });


        $(".more-row .more-item").click(function (e) {
            $(this).children("ul").toggle();
            $(this).toggleClass("active")
            e.stopPropagation();


        });

        $(".more-row .more-item ul").click(function (e) {
            e.stopPropagation();
        });

        $(document).click(function () {
            $(".more-row .more-item ul").hide();
            $(".more-row .more-item").removeClass("active")
        });
    </script>
    <script>

        $(".list-filter").click(function (e) {
            if ($(this).hasClass('active')) {
                $(".list-filter ul").hide();
                $(" .list-filter ").removeClass("active")
            } else {
                $(".list-filter ul").hide();
                $(" .list-filter ").removeClass("active")
                $(this).addClass("active")
                $(this).children("ul").toggle();
            }


            e.stopPropagation();


        });

        $(".list-filter ul").click(function (e) {
            e.stopPropagation();
        });

        $(document).click(function () {
            $(".list-filter ul").hide();
            $(" .list-filter ").removeClass("active")
        });
    </script>

@endsection


