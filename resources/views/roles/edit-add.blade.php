@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
@stop

@section('content')
    <div class="row">
        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
            <nav class="navigation">
                <div class="right-top-nav flex-box">
                    <a class="flex-box back" href="{{ url()->previous() }}">
                        <img src="{{voyager_asset('icon/back.svg')}}">
                        {{ __('voyager::generic.return_to_list') }}

                    </a>
                    <button class="flex-box add-new" onclick="document.getElementById('contact_form').submit()">
                        <img src="{{voyager_asset('icon/change.svg')}}">
                        <p>
                            {{ __('voyager::generic.submit') }}
                        </p>
                    </button>
                </div>
            </nav>
        </div>


        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12">
                    <div class="row">
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <h5 class="title">
                                {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
                            </h5>
                        </div>
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="box">
                                <form id="contact_form" role="form" class="form"
                                      action="@if(isset($dataTypeContent->id)){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->id) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                                      method="POST" enctype="multipart/form-data">

                                    <!-- PUT Method if we are editing -->
                                @if(isset($dataTypeContent->id))
                                    {{ method_field("PUT") }}
                                @endif

                                <!-- CSRF TOKEN -->
                                    {{ csrf_field() }}
                                    <div class="row">
                                        @foreach($dataType->addRows as $row)

                                            <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
                                                <div class="input-row">
                                                    <label>
                                                        {{ $row->getTranslatedAttribute('display_name') }}
                                                    </label>
                                                    {!! Voyager::formField($row, $dataType, $dataTypeContent) !!}

                                                </div>
                                            </div>
                                        @endforeach

                                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                                            <h5 class="title">
                                                {{ __('voyager::generic.permissions') }}
                                            </h5>
                                        </div>
                                        <?php
                                        $role_permissions = (isset($dataTypeContent)) ? $dataTypeContent->permissions->pluck('key')->toArray() : [];
                                        ?>
                                        @foreach(Voyager::model('Permission')->all()->groupBy('table_name') as $table => $permission)
                                            @if(Auth::user()->role->type == 1 && !empty($table) && !Auth::user()->hasPermission("read_".$table) && !Auth::user()->hasPermission("browse_".$table))
                                                @php continue; @endphp
                                            @endif
                                            <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                                                <h5 class="title-level2">
                                                    {{\Illuminate\Support\Str::title(str_replace('_',' ', $table))}}
                                                </h5>
                                            </div>
                                            @foreach($permission as $perm)
                                                @if(Auth::user()->role->type == 1 && !Auth::user()->hasPermission($perm->key))
                                                    @php continue; @endphp
                                                @endif
                                                <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                                                    <div class="flex-box check-box-row">

                                                        <label class="switch">
                                                            <h6>
                                                                {{\Illuminate\Support\Str::title(str_replace('_', ' ', $perm->key))}}
                                                            </h6>
                                                            <input hidden name="permissions[]" type="checkbox" value="{{$perm->id}}" @if(count($role_permissions) && in_array($perm->key, $role_permissions)) checked @endif />
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                @endforeach

                                            @endforeach

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="row">
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <h5 class="title">
                                {{ __('voyager::generic.guide') }}
                            </h5>
                        </div>
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <p class="Description">
                                {{ __('voyager::generic.lorem_ipsum') }}

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

{{--@section('javascript')--}}
{{--    <script>--}}
{{--        $('document').ready(function () {--}}
{{--            $('.toggleswitch').bootstrapToggle();--}}
{{--            var inittype = $('select[name=type] option:selected').val();--}}
{{--            @if(!isset($dataTypeContent->id))--}}
{{--            if(inittype==0){--}}
{{--              $(".permissions input[type='checkbox']").prop('checked', true);--}}
{{--            }--}}
{{--            @endif--}}
{{--            @if(Auth::user()->role->type == 1)--}}
{{--              $('select[name=type] option[value=0]').remove().change();--}}
{{--            @endif--}}
{{--            $('select[name=type]').on('change', function(){--}}
{{--                var type = $(this).val();--}}
{{--                if(type==0){--}}
{{--                  $(".permissions input[type='checkbox']").prop('checked', true);--}}
{{--                }else if(type==1){--}}
{{--                  $(".permissions input[value=1]").prop('checked', true);--}}
{{--                  $(".permissions input[value=2]").prop('checked', false);--}}
{{--                  $(".permissions input[value=3]").prop('checked', false);--}}
{{--                  $(".permissions input[value=4]").prop('checked', false);--}}
{{--                  $(".permissions input[value=5]").prop('checked', false);--}}
{{--                  $(".permissions input[value=26]").prop('checked', false);--}}
{{--                  $(".permissions #r_menus").prop('checked', false).change();--}}
{{--                  $(".permissions #r_roles").prop('checked', true).change();--}}
{{--                  $(".permissions #r_settings").prop('checked', false).change();--}}
{{--                  $(".permissions #r_users").prop('checked', true).change();--}}
{{--                }else if(type==2){--}}
{{--                  $(".permissions input[value=1]").prop('checked', true);--}}
{{--                  $(".permissions input[value=2]").prop('checked', false);--}}
{{--                  $(".permissions input[value=3]").prop('checked', false);--}}
{{--                  $(".permissions input[value=4]").prop('checked', false);--}}
{{--                  $(".permissions input[value=5]").prop('checked', false);--}}
{{--                  $(".permissions input[value=26]").prop('checked', false);--}}
{{--                  $(".permissions #r_menus").prop('checked', false).change();--}}
{{--                  $(".permissions #r_roles").prop('checked', false).change();--}}
{{--                  $(".permissions #r_settings").prop('checked', false).change();--}}
{{--                  $(".permissions #r_users").prop('checked', false).change();--}}
{{--                }--}}
{{--            });--}}

{{--            $('.permission-group').on('change', function(){--}}
{{--                $(this).parent().siblings('ul').find("input[type='checkbox']").prop('checked', this.checked);--}}
{{--            });--}}

{{--            $('.permission-select-all').on('click', function(){--}}
{{--                $('ul.permissions').find("input[type='checkbox']").prop('checked', true);--}}
{{--                return false;--}}
{{--            });--}}

{{--            $('.permission-deselect-all').on('click', function(){--}}
{{--                $('ul.permissions').find("input[type='checkbox']").prop('checked', false);--}}
{{--                return false;--}}
{{--            });--}}

{{--            function parentChecked(){--}}
{{--                $('.permission-group').each(function(){--}}
{{--                    var allChecked = true;--}}
{{--                    $(this).parent().siblings('ul').find("input[type='checkbox']").each(function(){--}}
{{--                        if(!this.checked) allChecked = false;--}}
{{--                    });--}}
{{--                    $(this).prop('checked', allChecked);--}}
{{--                });--}}
{{--            }--}}

{{--            parentChecked();--}}

{{--            $('.the-permission').on('change', function(){--}}
{{--                parentChecked();--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}
{{--@stop--}}
